import { httpClient } from "./http-client";

// login
export const loginRequest = (data) => {
  return httpClient.post("/login", data);
};
 
// register
export const registerRequest = (data) => {
  return httpClient.post("/register", data);
};

// forgot
export const forgotRequest = (data) => {
  return httpClient.post("/forgot", data);
};

// reset
export const resetRequest = (data) => {
  return httpClient.post("/reset", data);
};

// verify
export const verifyRequest = (data) => {
  return httpClient.post("/verify", data);
};

// resend
export const resendRequest = (data) => {
  return httpClient.post("/resend", data);
};
