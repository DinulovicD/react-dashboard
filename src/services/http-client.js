import axios from "axios";

export const baseURL = "https://sandbox-backend.itcentar.rs/api";

export const httpClient = axios.create({
  baseURL: `${baseURL}`,
  timeout: 1000,
  headers: {
    "Content-Type": "application/json",
  },
}); 

httpClient.interceptors.request.use((config) => { 
  const token = localStorage.getItem("token");
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

httpClient.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (!error || !error.response || !error.response.data) {
      return Promise.reject(error);
    }

    // parse message from response
    const { message } = error?.response?.data;

    if (message) {
      // check if message has "Request validation of body failed, because: " and remove it
      const regex = /Request validation of body failed, because: /;
      const messageWithoutText = message.replace(regex, "");

      // split by ","
      const messageArray = messageWithoutText.split(",");

      // remove " "
      const messageArrayWithoutSpaces = messageArray.map((item) => item.trim());

      // return promise
      return Promise.reject({
        messages: messageArrayWithoutSpaces,
        ...error.response.data, 
      });
    } else {
      return Promise.reject(error);
    }
  }
);
