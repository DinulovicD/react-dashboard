import { httpClient } from "./http-client";

export const getUsersRequest = () => {
  return httpClient.get("/users"); 
};

export const deleteUserRequest = (id) => {
  return httpClient.delete(`/users/${id}`);
};

export const updateUserRequest = (id, data) => {
  return httpClient.patch(`/users/${id}`, data);
};

export const createUserRequest = (data) => {
  return httpClient.post("/users", data);
};
