import { httpClient } from "./http-client";

export const getChyronsRequest = () => {
  return httpClient.get("/chyrons"); 
};

export const deleteUserRequest = (id) => {
  return httpClient.delete(`/chyrons/${id}`);
};

export const updateUserRequest = (id, data) => {
  return httpClient.patch(`/chyrons/${id}`, data);
};

export const createUserRequest = (data) => {
  return httpClient.post("/chyrons", data);
};
