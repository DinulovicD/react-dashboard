import React, { useContext } from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";

import { AppLayout } from './components/layout/AppLayout';

// CSS
import 'antd/dist/antd.css';
import 'antd-css-utilities/utility.min.css';

//Context
import { UserContext, UserProvider } from "./contexts/user.context";

//Components
import Dashboard from './components/pages/Dashboard';
import Media from './components/pages/media/Media';
import Plugins from './components/pages/plugins/Plugins';
import Playlist from './components/pages/playlist/Playlist';
import Aplication from './components/pages/aplication/Aplication';
import Advertiser from './components/pages/advertiser/Advertiser';
import NotFound from './components/pages/NotFound';
import { Users } from './components/modules/users/users.module';
import { Companies } from './components/modules/companies/companies.module';
import { Reports } from './components/modules/reports/reports.module';
import { Chyrons } from './components/modules/chyrons/chyrons.module';
import { Devices } from './components/modules/devices/devices.module';
import { GroupDevices } from './components/modules/group-devices/group-devices.module';
import { MapDevices } from './components/modules/map-devices/map-devices.module';
import { SuperAdmin } from './components/modules/super-admin/super-admin.module';

import { Auth } from "./components/modules/auth/auth.module";
import { Spin } from "antd";

const AppRouter = () => {
  const { user, isAdmin, isLoading } = useContext(UserContext);

  if (isLoading)
    return (
      <div
        style={{
          height: "100vh",
          width: "100vw",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spin />
      </div>
    );

  return <>
    <Router>
      <Routes>

        {!user && (
          <Route path="/*" element={<Auth />} />
        )}

        {!!user && (
          <Route path="/" element={<AppLayout />}>
            <Route path="/" element={<Dashboard />} />
            <Route path="/media" element={<Media />} />
            <Route path="/plugins" element={<Plugins />} />
            <Route path="/playlist" element={<Playlist />} />
            <Route path="/users/*" element={<Users />} />
            <Route path="/companies/*" element={<Companies />} /> 
            <Route path="/chyrons/*" element={<Chyrons />} /> 
            <Route path="/devices/*" element={<Devices />} /> 
            <Route path="/devices/super-admin/*" element={<SuperAdmin />} /> 
            <Route path="/group-devices/*" element={<GroupDevices />} /> 
            <Route path="/map-devices/*" element={<MapDevices />} /> 
            <Route path="/aplication" element={<Aplication />} /> 
            <Route path="/advertiser" element={<Advertiser />} /> 
            <Route path="/reports/*" element={<Reports />} /> 
          </Route>
        )}

        <Route path="*" element={<NotFound />} />
        
      </Routes>
    </Router>
  </>
};

const App = () => {

  return (
    <UserProvider>
      <AppRouter />
    </UserProvider>
  );

};

export default App;
