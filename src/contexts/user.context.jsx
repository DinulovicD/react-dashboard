import React, { useState, useEffect, createContext } from "react";
import { getMeRequest } from "../services/users.service";

export const UserContext = createContext();

export const UserProvider = ({ children }) => { 
  const [user, setUser] = useState(null);
  const [isAdmin, setIsAdmin] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setUser(user);
  }, [user]);

  useEffect(() => {
    setIsLoading(false);
    const localUser = localStorage.getItem("user");
    setUser(JSON.parse(localUser));
    // if (localUser) {
    //   getMeRequest()
    //     .then(({ data }) => {
    //       setUser(data);
    //     }) 
    //     .catch(() => {
    //       // retry request
    //       getMeRequest().then(({ data }) => {
    //         setUser(data);
    //       });
    //     })
    //     .finally(() => {
    //       setIsLoading(false);
    //     });
    // } else {
    //   setIsLoading(false);
    // }
  }, []);

  const login = ({ user, token }) => {
    localStorage.setItem("user", JSON.stringify(user));
    localStorage.setItem("token", token);
    setUser(user);
  };

  const logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    setUser(null);
  };

  const update = (user) => {
    localStorage.setItem("user", JSON.stringify(user));
    setUser(user);
  };
 
  return (
    <UserContext.Provider
      value={{ user, setUser, login, logout, update, isAdmin, isLoading }}
    >
      {children}
    </UserContext.Provider>
  );
};
