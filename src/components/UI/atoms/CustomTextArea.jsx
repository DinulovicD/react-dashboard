import React from 'react';
import { Input } from 'antd';

const { TextArea } = Input;

const CustomTextArea = ({ field, form, ...props }) => {
  // console.log('Field', field);
  // console.log('Form', form);
  // console.log('Props', props);
    return ( 
      <> 
      
        <label className=''>
          {props.label} 
          <TextArea rows={5} {...field} {...props} /> 
        </label>
      </>
    );
  };
 
export default CustomTextArea; 