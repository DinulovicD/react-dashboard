import React from 'react'

//Antd
import { notification } from 'antd'

//Icons
import {
    CheckCircleOutlined,
  } from '@ant-design/icons';

function CustomSuccessNotification(props) {
    return (
        notification.open({
            message: "Izmen",
            description: "ds",
            icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
        })
    )
}

export default CustomSuccessNotification