import { Switch } from 'antd';
import React from 'react'

const CustomSwitch = (props) => { 
    return ( 
      <> 
        
        <div className="switch-components">
            <Switch {...props} />
            <span>{props.children}</span>
        </div>

      </>
    );
  };

export default CustomSwitch;