import React from 'react'
import { Input } from 'antd';

const { Search } = Input;

const CustomSearchInput = ({ field, form, ...props }) => {
  // console.log('Field', field);
  // console.log('Form', form);
  // console.log('Props', props);
    return ( 
      <> 
      
       <label>
         {props.label} 
         <Search {...field} {...props} />
       </label>

      </>
    );
  };

export default CustomSearchInput;