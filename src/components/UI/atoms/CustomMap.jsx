import React, { useEffect, useState } from 'react'
import PinImage from './../../../assets/images/pin.png';
import { GoogleMap, useLoadScript, Marker, InfoWindow, Autocomplete} from '@react-google-maps/api';
import { config } from '../../../config/config';
import { useFormikContext } from 'formik';


const libraries = ["places"];
const mapContainerStyle = {
    width: '100%',
    height: '500px'
}

const CustomMap = ({ field, form, ...props }) => {
    const FormikProps = useFormikContext();
    const autocomplete = null;

    useEffect(() => {
        FormikProps.setFieldValue('device_lat', position.lat);
        FormikProps.setFieldValue('device_lng', position.lng);
    }, []);

    const [center, setCenter] = useState({
        lat: 44.5204666,
        lng: 19.3838752,
    });

    const [position, setPosition] = useState({
        lat: 44.5204666,
        lng: 19.3838752,
    });

    const options = {
        styles: [{
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#444444"
            }]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{
                "color": "#f2f2f2"
            }]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [{
                "saturation": -100
            },
            {
                "lightness": 45
            }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [{
                "visibility": "simplified"
            }]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#ffffff"
            }]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [{
                "color": "#dde6e8"
            },
            {
                "visibility": "on"
            }
            ]
        }
        ],
    }

    const successGetLocation = (pos) => {
        const { latitude, longitude } = pos.coords;
        FormikProps.setFieldValue('device_lat', latitude);
        FormikProps.setFieldValue('device_lng', longitude);

        setPosition({
            lat: latitude,
            lng: longitude
        })
    }

    const errorGetLocation = (err) => {
        console.log(err.code);
        console.log(err.message);
    }

    const onLoad = (autocomplete) => { 
        navigator?.geolocation.getCurrentPosition(successGetLocation, errorGetLocation);
        console.log('autocomplete', autocomplete);
    }

    const onPlaceChanged = () => {
        if (autocomplete !== null) {
            console.log('Place', autocomplete.getPlace())
          } else {
            console.log('Autocomplete is not loaded yet!')
          }
    }

    const onDragEnd = (e) => {
        // console.log(e);
        const lat = e.latLng.lat();
        const lng = e.latLng.lng();
        FormikProps.setFieldValue('device_lat', lat);
        FormikProps.setFieldValue('device_lng', lng);
    }

    const { isLoaded, loadError } = useLoadScript({
        googleMapsApiKey: config.GOOGLE_MAPS_API,
        libraries,
    });

    const divStyle = {
        background: `white`,
        border: `1px solid #ccc`,
        padding: 15
    }

    const [openInfoWindow, setOpenInfoWindow] = useState(false);

    const onInfoWindowClose = () => {
        setOpenInfoWindow(false)
    }

    const onMarkerClick = () => {
        setOpenInfoWindow(true)
    }

    if (loadError) return "Error loading maps";
    if (!isLoaded) return "Loading maps";

    return (
        <>
            <GoogleMap
                mapContainerStyle={mapContainerStyle}
                zoom={7}
                center={center}
                options={options}
            >

                <Autocomplete
                    onLoad={onLoad}
                    onPlaceChanged={onPlaceChanged}
                >
                    <input
                    type="text"
                    placeholder="Customized your placeholder"
                    style={{
                        boxSizing: `border-box`,
                        border: `1px solid transparent`,
                        width: `240px`,
                        height: `32px`,
                        padding: `0 12px`,
                        borderRadius: `3px`,
                        boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                        fontSize: `14px`,
                        outline: `none`,
                        textOverflow: `ellipses`,
                        position: "absolute",
                        left: "50%",
                        marginLeft: "-120px"
                    }}
                    />
                </Autocomplete>

                <Marker
                    icon={PinImage}
                    draggable={true}
                    onLoad={onLoad}
                    onDragEnd={onDragEnd}
                    onClick={onMarkerClick}
                    position={position}
                >
                    {openInfoWindow &&
                    <InfoWindow
                        position={position}
                        onCloseClick={onInfoWindowClose}
                    >
                        <div style={divStyle}>
                            <h1>InfoWindow</h1>
                        </div>
                    </InfoWindow>}

                </Marker>

            </GoogleMap>
        </>
    );
};

export default CustomMap;