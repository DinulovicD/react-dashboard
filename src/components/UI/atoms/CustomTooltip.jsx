import { Tooltip } from 'antd';
import React from 'react'

const CustomTooltip = (props) => { 
    return ( 
      <> 
        
        <Tooltip {...props}>{props.children}</Tooltip>

      </>
    );
  };

export default CustomTooltip;