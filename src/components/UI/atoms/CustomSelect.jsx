import React from 'react'
import { Select } from 'antd';

const CustomSelect = ({ field, form, ...props }) => {
//   console.log('Field', field);
//   console.log('Form', form);
//   console.log('Props', props);
    return ( 
      <> 
       
         <Select {...props} {...field}>
             {props.children}
         </Select>

      </>
    );
  };

export default CustomSelect;