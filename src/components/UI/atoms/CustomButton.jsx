import React from 'react'
import { Button } from 'antd';

const CustomButton = ({ field, form, ...props }) => {
//   console.log('Field', field);
//   console.log('Form', form);
//   console.log('Props', props);
    return ( 
      <> 
        
         <Button {...props} {...field}> {props.children} </Button>

      </>
    );
  };

export default CustomButton;