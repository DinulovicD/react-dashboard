import { Popconfirm } from 'antd';
import React from 'react'

const CustomPopConfirm = (props) => { 
    return ( 
      <> 
        
        <Popconfirm okText="Obriši" cancelText="Odustani" {...props}>{props.children}</Popconfirm>

      </>
    );
  };
 
export default CustomPopConfirm;