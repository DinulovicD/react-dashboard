import React from 'react';
import { NavLink, useLocation } from 'react-router-dom';

//Icons
import {
    HomeOutlined,
} from '@ant-design/icons';
import { Breadcrumb } from 'antd';

const CustomBreadcrumbs = (props) => {

    const location = useLocation();

    //Breadcrumbs
    const breadcrumbs = () => {
        const tempLocation = location.pathname.split('/').filter(t => t != '');
        return (
            tempLocation.map((loc, index) => {
                const sliceLocation = tempLocation.slice(0, index + 1)
                loc = loc.replace(/-/g, ' ');
                return <Breadcrumb.Item key={index}> <NavLink to={'/' + sliceLocation.join('/')}> {loc.replace(/^[a-z]/, function (m) { return m.toUpperCase() })} </NavLink> </Breadcrumb.Item>
            })
        )
    }

    return (
        <>

            <Breadcrumb>
                <Breadcrumb.Item><NavLink to="/"><HomeOutlined /></NavLink></Breadcrumb.Item>
                {breadcrumbs()}
            </Breadcrumb>

        </>
    );
};

export default CustomBreadcrumbs;