import React from 'react'

const CustomBadge = (props) => { 
    return ( 
      <> 
        
         <div className="badge">{props.children}</div>

      </>
    );
  };

export default CustomBadge;