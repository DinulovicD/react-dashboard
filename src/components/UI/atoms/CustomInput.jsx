import React from 'react'
import { Input as InputAnt } from 'antd';

const CustomInput = ({ field, form, ...props }) => { 
  
    return ( 
      <> 
      
       <label >
         {props.label} 
         <InputAnt {...field} {...props} />
       </label>

      </>
    );
  };

export default CustomInput;