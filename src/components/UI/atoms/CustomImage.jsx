import React from 'react'

const CustomImage = (props) => { 
    return ( 
      <> 
        
        <img {...props} loading="lazy" className='img-fluid'  />

      </>
    );
  };

export default CustomImage;