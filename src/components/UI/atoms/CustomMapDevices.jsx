import React, { useEffect, useState } from 'react'
import PinImage from './../../../assets/images/pin.png';
import { GoogleMap, useLoadScript, Marker, InfoWindow} from '@react-google-maps/api';
import { config } from '../../../config/config';
import { useFormikContext } from 'formik';


const libraries = ["places"];
const mapContainerStyle = {
    width: '100%',
    height: '700px'
}

const CustomMap = ({ field, form, ...props }) => {  
 

    const [center, setCenter] = useState({
        lat: 44.5204666,
        lng: 19.3838752,
    });

    const [position, setPosition] = useState({
        lat: 44.5204666,
        lng: 19.3838752,
    });

    const options = {
        styles: [{
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#444444"
            }]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{
                "color": "#f2f2f2"
            }]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [{
                "saturation": -100
            },
            {
                "lightness": 45
            }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [{
                "visibility": "simplified"
            }]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#ffffff"
            }]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [{
                "color": "#dde6e8"
            },
            {
                "visibility": "on"
            }
            ]
        }
        ],
    }


    const { isLoaded, loadError } = useLoadScript({
        googleMapsApiKey: config.GOOGLE_MAPS_API,
        libraries,
    });

    const divStyle = {
        background: `white`,
        border: `1px solid #ccc`,
        padding: 15
    }

    const [openInfoWindow, setOpenInfoWindow] = useState(false);

    const onInfoWindowClose = () => {
        setOpenInfoWindow(false)
    }

    const onMarkerClick = () => {
        setOpenInfoWindow(true)
    }

    if (loadError) return "Error loading maps";
    if (!isLoaded) return "Loading maps";

    return (
        <>
            <GoogleMap
                mapContainerStyle={mapContainerStyle}
                zoom={7}
                center={center}
                options={options}
            >

                <Marker
                    icon={PinImage}
                    draggable={false} 
                    onClick={onMarkerClick}
                    position={position}
                >
                    {openInfoWindow &&
                    <InfoWindow
                        position={position}
                        onCloseClick={onInfoWindowClose}
                    >
                        <div style={divStyle}>
                            <h1>InfoWindow</h1>
                        </div>
                    </InfoWindow>}

                </Marker>

            </GoogleMap>
        </>
    );
};

export default CustomMap;