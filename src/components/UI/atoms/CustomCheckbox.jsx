import React from 'react'
import { Checkbox } from 'antd';

const CustomCheckbox = ({ field, form, ...props }) => {
  
    return ( 
      <> 
 
         <Checkbox {...props} {...field} > {props.label} </Checkbox>
       
      </>
    );
  };

export default CustomCheckbox;