import React from 'react'
import { Radio } from 'antd';

const CustomRadio = ({ field, form, ...props }) => {
//   console.log('Field', field);
//   console.log('Form', form);
//   console.log('Props', props);
    return ( 
      <> 
       
         <Radio {...props} {...field}> {props.label} </Radio>

      </>
    );
  };

export default CustomRadio;