import React from 'react'

//Antd
import { Col, Row } from 'antd'
import CustomBreadcrumbs from '../atoms/CustomBreadcrumbs'

//Custom components

function CustomTopInfo(props) {
    return <>
        <Row gutter={[24, 24]} className="mb-5" justify='between'>
            <Col span={12}>
                <h1 className='page-title'>{props.children}</h1>
            </Col>
            <Col span={12} className="d-flex justify-end ">
                <CustomBreadcrumbs></CustomBreadcrumbs>
            </Col>
        </Row>
    </>
}

export default CustomTopInfo