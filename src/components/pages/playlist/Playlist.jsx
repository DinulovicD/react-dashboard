import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

//Ant 
import { Row, Col, Select, Button, Tabs, Modal, Skeleton } from 'antd';

//Icons
import {
  VideoCameraOutlined,
  DeleteOutlined,
  EditOutlined,
  DragOutlined,
  UsbOutlined,
  FileImageOutlined,
  QuestionCircleOutlined
} from '@ant-design/icons';
import { AiOutlinePlus, AiOutlineNumber } from "react-icons/ai";
import { BsSave } from "react-icons/bs";
import { BiTimer, BiMoviePlay } from 'react-icons/bi';

// Custom components
import CustomSelect from '../../UI/atoms/CustomSelect';
import CustomSearchInput from '../../UI/atoms/CustomSearchInput';
import CustomTooltip from '../../UI/atoms/CustomTooltip';
import CustomImage from '../../UI/atoms/CustomImage';
import CustomInput from '../../UI/atoms/CustomInput';
import CustomButton from '../../UI/atoms/CustomButton';

//
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { config } from '../../../config/config';
import axios from 'axios';
import CustomPopConfirm from '../../UI/atoms/CustomPopConfirm';
import CustomTopInfo from '../../UI/molecules/CustomTopInfo';


const Playlist = () => {

  const { TabPane } = Tabs;
  const { Option } = Select;

  //Use states
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalEditVisible, setIsModalEditVisible] = useState(false);
  const [isModalChangeTimeVisible, setIsModalChangeTimeVisible] = useState(false);
  const [playlistData, setPlaylistData] = useState([]);
  const [mediaData, setMediaData] = useState([]);
  const [playlistOrder, setPlaylistOrder] = useState(playlistData);
  const [isLoading, setIsLoading] = useState(false);
  const [showButtonPlaylist, setShowButtonPlaylist] = useState(false);

  //Use effects
  useEffect(() => {
    getAllPlaylist();
    getAllMedia();
    setIsLoading(true);

  }, []);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const showModalEdit = () => {
    setIsModalEditVisible(true);
  };

  const showModalChangeTime = () => {
    setIsModalChangeTimeVisible(true);
  };


  const handleOk = () => {
    setIsModalVisible(false);
    setIsModalEditVisible(false);
    setIsModalChangeTimeVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setIsModalEditVisible(false);
    setIsModalChangeTimeVisible(false);
  };

  //Get all playlist - AXIOS
  const getAllPlaylist = () => {
    return (
      axios.get(config.JSON_SERVER + 'plugins').then(function (response) {
        // handle success
        console.log(response);
        setPlaylistData(response.data);
        setPlaylistOrder(response.data);
        setTimeout(() => {
          setIsLoading(false);
        }, 500)

      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }

  //Get all media - AXIOS
  const getAllMedia = () => {
    return (
      axios.get(config.JSON_SERVER + 'media').then(function (response) {
        // handle success
        console.log(response);
        setMediaData(response.data);
        setTimeout(() => {
          setIsLoading(false);
        }, 500)

      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }


  // Playlist modal
  const addPlaylistModal = () => {
    return (
      <Modal centered title="Dodaj novu playlistu" visible={isModalVisible} okText="Sačuvaj"
        cancelText="Odustani" onOk={handleOk} onCancel={handleCancel}>
        <CustomInput label="Naziv playliste"></CustomInput>
      </Modal>
    )
  }

  // Playlist edit modal
  const editPlaylistModal = () => {
    return (
      <Modal centered title="Izmeni playlistu" visible={isModalEditVisible} okText="Sačuvaj"
        cancelText="Odustani" onOk={handleOk} onCancel={handleCancel}>
        <CustomInput label="Naziv playliste"></CustomInput>
      </Modal>
    )
  }

  // Playlist change time
  const changePlaylistTimeModal = () => {
    return (
      <Modal centered title="Promena trajanja" visible={isModalChangeTimeVisible} okText="Sačuvaj"
        cancelText="Odustani" onOk={handleOk} onCancel={handleCancel}  width={800}>

        <div className="playlist-item">
          <div className="image">
            <div className="image-type">
              <VideoCameraOutlined />
            </div>
            <CustomImage src="https://picsum.photos/200/300"></CustomImage>
          </div>
          <div className="info">
            <h4 className='card-title'>Test video</h4>
            <p className='m-0'><BiTimer /> 00:00:15</p>
          </div>
          <div className="icons">

            <CustomInput value="00:00:25" className="video-type" disabled></CustomInput>

            <CustomTooltip title="Dodaj">
              <Button icon={<BsSave />} />
            </CustomTooltip>
          </div>
        </div>

        <div className="playlist-item">
          <div className="image">
            <div className="image-type">
              <UsbOutlined />
            </div>
            <CustomImage src="https://picsum.photos/200/300"></CustomImage>
          </div>
          <div className="info">
            <h4 className='card-title'>Test plugin</h4>
            <p className='m-0'><BiTimer /> 00:00:15</p>
          </div>
          <div className="icons">

            <CustomInput value="10"></CustomInput>
            <CustomSelect
              showSearch
              placeholder="sec"
              optionFilterProp="children">
              <Option value="sec">sec</Option>
              <Option value="min">min</Option>
            </CustomSelect>

            <CustomTooltip title="Dodaj">
              <Button icon={<BsSave />} />
            </CustomTooltip>
          </div>
        </div>

        <div className="playlist-item">
          <div className="image">
            <div className="image-type">
              <FileImageOutlined />
            </div>
            <CustomImage src="https://picsum.photos/200/300"></CustomImage>
          </div>
          <div className="info">
            <h4 className='card-title'>Test image</h4>
            <p className='m-0'><BiTimer /> 00:00:15</p>
          </div>
          <div className="icons">

            <CustomInput value="10"></CustomInput>
            <CustomSelect
              showSearch
              placeholder="sec"
              optionFilterProp="children">
              <Option value="sec">sec</Option>
              <Option value="min">min</Option>
            </CustomSelect>

            <CustomTooltip title="Dodaj">
              <Button icon={<BsSave />} />
            </CustomTooltip>
          </div>
        </div>

        <div className="total-time"><strong>Ukupno vreme:</strong> 00:01:15</div>
      </Modal>
    )
  }

  //Handle drag
  const handleOnDragEnd = (result) => {

    console.log(result);

    if (!result.destination) return;

    const items = Array.from(playlistOrder);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);

    setShowButtonPlaylist(true);

    setPlaylistOrder(items);

  }

  // Content playlist
  const playlist = () => {
    return (
      <div className="card-box playlist-wrapper pa-0">
        <div className="playlist-header">
          <h3 className='card-title'>Playlist / master <div class="playlist-info"><p><BiTimer /> 00:02:45 </p> <p><AiOutlineNumber /> <span className="playlist-count">{playlistOrder.length} </span> stavki </p> </div></h3>

          <div className="icons">


            <CustomTooltip title="Izmeni trajanje" onClick={showModalChangeTime}>
              <Button icon={<BiTimer />} />
            </CustomTooltip>

            <CustomTooltip title="Pogledaj preview">
              <Button icon={<BiMoviePlay />} />
            </CustomTooltip>

            <CustomTooltip title="Izmeni playlistu">
              <Button icon={<EditOutlined />} onClick={showModalEdit} />
            </CustomTooltip>

            <CustomTooltip title="Obriši playlistu">
              <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                <Button icon={<DeleteOutlined />} />
              </CustomPopConfirm>
            </CustomTooltip>

          </div>
        </div>
        <Skeleton active loading={isLoading} className="pa-5" paragraph={{ rows: 6 }}>
          <DragDropContext onDragEnd={handleOnDragEnd}>
            <Droppable droppableId="playlist">
              {(provided) => (
                <div className="playlist-content" {...provided.droppableProps} ref={provided.innerRef}>

                  {playlistOrder.map((item, index) => {

                    const { id, name, key, media_size, image } = item;
                    return <>
                      <Draggable key={key.toString()} draggableId={id.toString()} index={index}>
                        {(provided) => (

                          <div className="playlist-item" ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                            <div className="image">
                              <div className="image-type">
                                <FileImageOutlined />
                              </div>
                              <CustomImage src={image}></CustomImage>
                            </div>
                            <div className="info">
                              <h4 className='card-title'>{name}</h4>
                              <p className='m-0'><BiTimer /> 00:00:15</p>
                            </div>
                            <div className="icons">
                              <CustomTooltip title="Obriši">
                                <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                                  <Button icon={<DeleteOutlined />} />
                                </CustomPopConfirm>
                              </CustomTooltip>

                              <CustomTooltip title="Premesti">
                                <Button icon={<DragOutlined />} />
                              </CustomTooltip>
                            </div>
                          </div>

                        )}
                      </Draggable>

                    </>

                  })
                  }

                  {provided.placeholder}
                  {showButtonPlaylist ? <Row gutter={[24, 24]} className='save-btn-playlist d-flex align-center'><Col span={12}><CustomButton type="primary" className="w-100 d-flex justify-center align-center">Sačuvaj</CustomButton></Col><Col span={12}><CustomButton className="w-100 d-flex justify-center align-center" onClick={() => window.location.reload()}>Odustani</CustomButton></Col></Row> : null}
                </div>


              )}
            </Droppable>
          </DragDropContext>
        </Skeleton>
      </div>
    )
  }

  // Media
  const media = () => {
    return (
      <div className="card-box media-wrapper pa-0">
        <div className="playlist-header">
          <h3 className='card-title'>Media</h3>
          <Tabs type="card" className="playlist-tabs">
            <TabPane
              tab={
                <span>
                  Sve
                </span>
              }
              key="1"
            >
            </TabPane>

            <TabPane
              tab={
                <span>
                  <FileImageOutlined />
                  Slike
                </span>
              }
              key="2"
            >
            </TabPane>

            <TabPane
              tab={
                <span>
                  <VideoCameraOutlined />
                  Video
                </span>
              }
              key="3"
            >
            </TabPane>

            <TabPane
              tab={
                <span>
                  <UsbOutlined />
                  Plugins
                </span>
              }
              key="4"
            >
            </TabPane>
          </Tabs>
        </div>

        <CustomSearchInput placeholder="Pretražite.."></CustomSearchInput>

        <Skeleton active loading={isLoading} className="pa-5" paragraph={{ rows: 6 }}>
          <div className="playlist-content media-content mt-5" >

            <div className="playlist-item">
              <div className="image">
                <div className="image-type">
                  <VideoCameraOutlined />
                </div>
                <CustomImage src="https://picsum.photos/200/300"></CustomImage>
              </div>
              <div className="info">
                <h4 className='card-title'>Test video</h4>
                <p className='m-0'><BiTimer /> 00:00:15</p>
              </div>
              <div className="icons">

                <CustomInput value="00:00:25" className="video-type" disabled></CustomInput>

                <CustomTooltip title="Dodaj">
                  <Button icon={<BsSave />} />
                </CustomTooltip>
              </div>
            </div>

            <div className="playlist-item">
              <div className="image">
                <div className="image-type">
                  <UsbOutlined />
                </div>
                <CustomImage src="https://picsum.photos/200/300"></CustomImage>
              </div>
              <div className="info">
                <h4 className='card-title'>Test plugin</h4>
                <p className='m-0'><BiTimer /> 00:00:15</p>
              </div>
              <div className="icons">

                <CustomInput value="10"></CustomInput>
                <CustomSelect
                  showSearch
                  placeholder="sec"
                  optionFilterProp="children">
                  <Option value="sec">sec</Option>
                  <Option value="min">min</Option>
                </CustomSelect>

                <CustomTooltip title="Dodaj">
                  <Button icon={<BsSave />} />
                </CustomTooltip>
              </div>
            </div>

            <div className="playlist-item">
              <div className="image">
                <div className="image-type">
                  <FileImageOutlined />
                </div>
                <CustomImage src="https://picsum.photos/200/300"></CustomImage>
              </div>
              <div className="info">
                <h4 className='card-title'>Test image</h4>
                <p className='m-0'><BiTimer /> 00:00:15</p>
              </div>
              <div className="icons">

                <CustomInput value="10"></CustomInput>
                <CustomSelect
                  showSearch
                  placeholder="sec"
                  optionFilterProp="children">
                  <Option value="sec">sec</Option>
                  <Option value="min">min</Option>
                </CustomSelect>

                <CustomTooltip title="Dodaj">
                  <Button icon={<BsSave />} />
                </CustomTooltip>
              </div>
            </div>


            {mediaData.map((item, index) => {
              const { id, name, key, media_size, image } = item;
              return <>

                <div className="playlist-item">
                  <div className="image">
                    <div className="image-type">
                      <VideoCameraOutlined />
                    </div>
                    <CustomImage src={image}></CustomImage>
                  </div>
                  <div className="info">
                    <h4 className='card-title'>{name}</h4>
                    <p className='m-0'><BiTimer /> 00:00:15</p>
                  </div>
                  <div className="icons">

                    <CustomInput value="10"></CustomInput>
                    <CustomSelect
                      showSearch
                      placeholder="sec"
                      optionFilterProp="children">
                      <Option value="sec">sec</Option>
                      <Option value="min">min</Option>
                    </CustomSelect>

                    <CustomTooltip title="Dodaj">
                      <Button icon={<BsSave />} />
                    </CustomTooltip>
                  </div>
                </div>

              </>

            })
            }

          </div>
        </Skeleton>

      </div >
    )
  }

  return <>

    <CustomTopInfo>Playlist</CustomTopInfo>


    <Row gutter={[24, 24]}>
      <Col span={20}>
        <Tabs type="card" >
          <TabPane
            tab={
              <span>
                Master
              </span>
            }
            key="1"
          >
          </TabPane>

          <TabPane
            tab={
              <span>
                Playlist 2
              </span>
            }
            key="2"
          >
          </TabPane>

          <TabPane
            tab={
              <span>
                Playlist 3
              </span>
            }
            key="3"
          >
          </TabPane>

          <TabPane
            tab={
              <span>
                Playlist 4
              </span>
            }
            key="4"
          >
          </TabPane>

          <TabPane
            tab={
              <span>
                Playlist 5
              </span>
            }
            key="5"
          >
          </TabPane>

          <TabPane
            tab={
              <span>
                Playlist 6
              </span>
            }
            key="6"
          >
          </TabPane>

          <TabPane
            tab={
              <span>
                Playlist 7
              </span>
            }
            key="7"
          >
          </TabPane>
        </Tabs>
      </Col>

      <Col span={4} className="d-flex justify-end">
        <Button type="primary" onClick={showModal} className='d-flex align-center'> Dodaj novu playlistu <AiOutlinePlus /></Button>
      </Col>
    </Row>

    <Row gutter={[24, 24]}>
      <Col span={12}>{playlist()}</Col>
      <Col span={12}>{media()}</Col>
    </Row>

    {addPlaylistModal()}
    {editPlaylistModal()}
    {changePlaylistTimeModal()}

  </>;
};

export default Playlist;
