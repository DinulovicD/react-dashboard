import React, { useEffect, useState } from 'react';
import axios from 'axios';

//Antd
import { Table, Select, Row, Col, Input, Button, Spin } from 'antd';
import {
  ReloadOutlined,
} from '@ant-design/icons';

// Custom components
import CustomSelect from '../../UI/atoms/CustomSelect';
import CustomSearchInput from '../../UI/atoms/CustomSearchInput';
import { config } from '../../../config/config';
import CustomTopInfo from '../../UI/molecules/CustomTopInfo';



const Advertiser = () => {


  const { Option } = Select;
  const { Search } = Input;


  //Use states
  const [data, setData] = useState([]);
  const [company, setCompany] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [options, setOptions] = useState([]);
  const [filters, setFilters] = useState({});

  //Use effects
  useEffect(() => {

    //Advertiser
    allAdvertisers();
    setIsLoading(true);

    //Companies
    allCompanies();

  }, []);

  useEffect(() => {
    allAdvertisers();
  }, [filters])


  //Get all advertisers - AXIOS
  const allAdvertisers = () => {
    return (
      axios.get(config.JSON_SERVER + 'advertiser', { params: filters }).then(function (response) {
        // console.log(response);
        setData(response.data.map(v => { return { ...v, key: v.id } }));
        setTimeout(() => {
          setIsLoading(false);
        }, 500)


      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }

  //Get all compines - AXIOS
  const allCompanies = () => {
    return (
      axios.get(config.JSON_SERVER + 'companies').then(function (response) {
        // handle success
        console.log(response);
        setCompany(response.data);

        setOptions(response.data.map(v => {

          return ({
            label: v.name,
            value: v.name
          });
        }));

      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }


  //Search devices
  const onSearchDevices = (value) => {
    // console.log(value);
    setFilters({
      ...filters,
      device_like: value
    })
  }


  //On change company filter
  const onChangeCompanyFilter = (value) => {

    setFilters({
      ...filters,
      company_like: value
    });

  }


  //Table columns
  const columns = [
    {
      title: 'Kompanija',
      dataIndex: 'company',
      key: 'company',
      filters: [
        {
          text: 'test company',
          value: 'test company',
        },
        {
          text: 'test company fake',
          value: 'test company fake',
        },
        {
          text: 'Samsung',
          value: 'Samsung',
        },
        {
          text: 'Domario',
          value: 'Domario',
        },
      ],
      onFilter: (value, record) => record.company.indexOf(value) === 0,
      sorter: (a, b) => a.company.length - b.company.length,
      sortDirections: ['descend'],
    },
    {
      title: 'uređaj',
      dataIndex: 'device',
      key: 'device',
    },
    {
      title: 'Grupa',
      dataIndex: 'group',
      key: 'group',
    },
    {
      title: 'Plejlista',
      dataIndex: 'playlist',
      key: 'playlist',
    },
    {
      title: 'Oglašivač',
      dataIndex: 'advertisers',
      key: 'advertisers',
    },
  ];


  const onChangeTable = (pagination, filters, sorter, extra) => {
    console.log('params', pagination, filters, sorter, extra);
  }

  const resetFilters = () => {
    setFilters({});
  }

  //Advertiser filter
  const advertiserFilters = () => {
    return (
      <Row gutter={[24, 24]} className='mb-4' justify='end'>

        {/* <h2>Reklamiranje</h2>  */}
        <Col flex="auto">
          <CustomSelect
            style={{ width: '100%' }}
            showSearch
            placeholder="Izaberite kompaniju"
            optionFilterProp="children"
            options={options}
            onChange={onChangeCompanyFilter}
          >
          </CustomSelect>
        </Col>
        <Col flex="auto"><CustomSearchInput onSearch={onSearchDevices} placeholder="Pretraži uređaje" /></Col>
        <Col flex="auto"><CustomSearchInput placeholder="Pretraži grupe" /></Col>
        <Col flex="auto"><CustomSearchInput placeholder="Pretraži oglašivače" /></Col>
        <Col><Button icon={<ReloadOutlined />} onClick={() => resetFilters()} /></Col>
      </Row>
    );
  }

  // Advertiser table
  const advertiserTable = () => {
    return (
      <Table columns={columns} loading={isLoading} bordered dataSource={data} onChange={onChangeTable} />
    )
  }


  return <>
    
    <CustomTopInfo>Reklamiranje</CustomTopInfo>

    <section className='card-box'>
      {advertiserFilters()}

      {advertiserTable()}
    </section>

  </>;
};

export default Advertiser;
