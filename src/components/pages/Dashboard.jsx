import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';


//Antd
import { Tabs, Card, Modal, Row, Col, Table, Space, Button, Skeleton, Progress } from 'antd';
import { Column } from '@ant-design/plots';

//Custom   
import CustomTooltip from '../UI/atoms/CustomTooltip';
import CustomPopConfirm from '../UI/atoms/CustomPopConfirm';
import CustomButton from '../UI/atoms/CustomButton';
import CustomImage from '../UI/atoms/CustomImage';

//Context
import { UserContext } from "../../contexts/user.context";


//Icons
import {
    EditOutlined,
    DeleteOutlined,
    QuestionCircleOutlined,
} from '@ant-design/icons';
import { FiUsers, FiMonitor } from 'react-icons/fi';
import { RiBuilding4Line } from 'react-icons/ri';
import userInfoImage from './../../assets/images/profile-img.png';

//Avatar image
import AvatarImage from './../../assets/images/deki.jpg';
import CustomTopInfo from '../UI/molecules/CustomTopInfo';


const Dashboard = () => {

    const { TabPane } = Tabs;
    const { Meta } = Card;

    const { user } = useContext(UserContext);

    //Use states
    const [isLoading, setIsLoading] = useState(false);
    const [state, setState] = useState({
        loading: true,
    });

    const onChange = checked => {
        setState({ loading: !checked });
    };

    const { loading } = state;

    // Modal
    const [isModalVisible, setIsModalVisible] = useState(false);
    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //Column chart
    const dataColumn = [
        {
            type: 'Lorem',
            value: 0.16,
        },
        {
            type: 'Ipsum',
            value: 0.125,
        },
        {
            type: 'Dolor',
            value: 0.24,
        },
        {
            type: 'Iset',
            value: 0.19,
        },
        {
            type: 'Amen',
            value: 0.22,
        },
        {
            type: 'Advertiser',
            value: 0.05,
        },
        {
            type: 'Users',
            value: 0.01,
        },
        {
            type: 'Company',
            value: 0.015,
        },
    ];
    const paletteSemanticRed = 'red';
    const brandColor = '#1890ff';
    const configColumn = {
        data: dataColumn,
        xField: 'type',
        yField: 'value',
        seriesField: '',
        color: ({ type }) => {
            if (type === '10-30分' || type === '30+分') {
                return paletteSemanticRed;
            }

            return brandColor;
        },
        label: {
            content: (originData) => {
                const val = parseFloat(originData.value);

                if (val < 0.05) {
                    return (val * 100).toFixed(1) + '%';
                }
            },
            offset: 10,
        },
        legend: false,
        xAxis: {
            label: {
                autoHide: true,
                autoRotate: false,
            },
        },
        minColumnWidth: 20,
        maxColumnWidth: 20,
    };


    //Headings table
    const columns = [
        {
            title: 'Naziv',
            dataIndex: 'naziv',
            key: 'naziv',
        },
        {
            title: 'Tip',
            dataIndex: 'tip',
            key: 'tip',
        },
        {
            title: 'Adresa',
            dataIndex: 'adresa',
            key: 'adresa',
        },
        {
            title: 'Grupa',
            dataIndex: 'grupa',
            key: 'grupa',
        },
        {
            title: 'Plejlista',
            dataIndex: 'plejlista',
            key: 'plejlista',
        },
        {
            title: 'Akcije',
            key: 'akcije',
            width: '10%',
            render: (user) => (
                <>
                    <Space size="middle">
                        <CustomTooltip title="Izmeni"><Link to="/device/edit/1"><Button className="mr-2 d-flex align-center justify-center" icon={<EditOutlined />} /></Link></CustomTooltip>

                        <CustomTooltip title="Obriši">
                            <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                                <Button className="mr-2 d-flex align-center justify-center" icon={<DeleteOutlined />} />
                            </CustomPopConfirm>
                        </CustomTooltip>
                    </Space>
                </>
            )
        },
    ];

    const dataTable = [
        {
            key: '1',
            naziv: 'test company fake',
            tip: 'fake test android box 1',
            adresa: 'group test fake 4',
            grupa: 'merc',
            plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
        },

        {
            key: '2',
            naziv: 'test company fake',
            tip: 'fake test android box 1',
            adresa: 'group test fake 4',
            grupa: 'merc',
            plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
        },

        {
            key: '3',
            naziv: 'test company fake',
            tip: 'fake test android box 1',
            adresa: 'group test fake 4',
            grupa: 'merc',
            plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
        },

        {
            key: '4',
            naziv: 'test company fake',
            tip: 'fake test android box 1',
            adresa: 'group test fake 4',
            grupa: 'merc',
            plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
        },

        {
            key: '5',
            naziv: 'test company fake',
            tip: 'fake test android box 1',
            adresa: 'group test fake 4',
            grupa: 'merc',
            plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
        },

        {
            key: '6',
            naziv: 'test company fake',
            tip: 'fake test android box 1',
            adresa: 'group test fake 4',
            grupa: 'merc',
            plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
        },


    ];


    /*
    *   Main content
    */
    const countData = () => {
        return (
            <Row gutter={[24, 24]} className="mb-5">

                <Col span={8}>
                    <div className="card-box dashbord-count">
                        <FiUsers />
                        <div className="info">
                            <p>Korisnici</p>
                            <h2>23</h2>
                        </div>
                    </div>
                </Col>
                <Col span={8}>
                    <div className="card-box dashbord-count">
                        <RiBuilding4Line />
                        <div className="info">
                            <p>Kompanije</p>
                            <h2>57</h2>
                        </div>
                    </div>
                </Col>
                <Col span={8}>
                    <div className="card-box dashbord-count">
                        <FiMonitor />
                        <div className="info">
                            <p>uređaji</p>
                            <h2>38</h2>
                        </div>
                    </div>
                </Col>
                {/* <Col span={6}>
                    <div className="card-box dashbord-count">
                        <BsMusicNoteList />
                        <div className="info">
                            <p>Playliste</p>
                            <h2>129</h2>
                        </div>
                    </div>
                </Col> */}
            </Row>
        )
    }

    const latestDataTabs = () => {
        return (
            <div className="card-box card-container mt-5">
                <Tabs className='latestTabs' type="card">

                    <TabPane
                        tab={
                            <span>
                                Korisnici
                            </span>
                        }
                        key="1"
                    >
                        <Table columns={columns} pagination={false} bordered dataSource={dataTable} />

                        <CustomButton type="primary" className="mt-4 mx-auto d-flex align-center">Pogledaj više </CustomButton>

                    </TabPane>

                    <TabPane
                        tab={
                            <span>
                                Kompanije
                            </span>
                        }
                        key="2"
                    >
                        Video
                    </TabPane>

                    <TabPane
                        tab={
                            <span>
                                uređaji
                            </span>
                        }
                        key="3"
                    >
                        Slike
                    </TabPane>

                    <TabPane
                        tab={
                            <span>
                                Playliste
                            </span>
                        }
                        key="4"
                    >
                        Slike
                    </TabPane>
                </Tabs>
            </div>

        )
    }

    const userInfo = () => {
        return (
            <Row gutter={[24, 24]}>
                <Col span={24}>
                    <div className="card-box user-info">
                        <div className="image">
                            <h3 className='card-title'>Dobrodošli</h3>
                            <p>OneView Dashboard</p>
                            <img src={userInfoImage} alt="" className="img-fluid" />
                        </div>
                        <div className="info">
                            <div className="d-flex">
                                <div className="col">
                                    <CustomImage src={AvatarImage}></CustomImage>
                                    <h3 className="card-title mb-0 mt-3">{user.name}</h3>
                                    <p className="fc-grey mb-4">IT Centar</p>
                                    <CustomButton type="primary" className="ant-btn-sm ml-0 mt-4 mx-auto d-flex align-center">Pogledaj profil </CustomButton>
                                </div>
                                <div className="col">
                                    <p>Isteka licence: 24.03.2023</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </Col>
                <Col span={24}>
                    <div className="card-box justify-center">
                        <h3 className='card-title'>Prostor na disku</h3>
                        <div className="d-flex">
                            <div className="col-7 d-flex flex-column">
                                <p>Za dodatni prostor kontaktirajte nas</p>
                                <CustomButton type="primary" className="ant-btn-sm ml-0 mt-4 mx-auto d-flex align-center">Kontakt </CustomButton>

                            </div>
                            <div className="col-3 d-flex justify-end align-end flex-column">
                                <Progress type="circle" percent={75} />
                                <div className="progress-used-space mt-2"> <span className="progress-label"></span> Zauzet prostor</div>
                            </div>
                        </div>
                    </div>
                </Col>
            </Row>
        )
    }

    const chartsPreview = () => {
        return (
            <Row gutter={[24, 24]}>
                <Col span={24}>
                    <div className="card-box">
                        <h3 className='card-title mb-5'>Prikaz statistike</h3>
                        <Column className='column-chart' height={350} width={1000} {...configColumn} />
                    </div>
                </Col>
            </Row>
        )
    }


  


    return <>
        <Skeleton active loading={isLoading} paragraph={{ rows: 6 }}>

        <CustomTopInfo>Dashboard</CustomTopInfo>


            <Row gutter={[24, 24]}>
                <Col span={9}>
                    {userInfo()}
                </Col>
                <Col span={15}>
                    {countData()}
                    {chartsPreview()}
                </Col>
            </Row>

            {latestDataTabs()}

            <Modal centered title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                <p>Some contents...</p>
                <p>Some contents...</p>
                <p>Some contents...</p>
            </Modal>
        </Skeleton>
    </>;
};

export default Dashboard;
