import React, { useEffect, useState } from 'react';
import axios from 'axios';

//Antd
import { Upload, notification, Row, Col, message, Collapse, Skeleton } from 'antd';
import {
  CheckCircleOutlined,
} from '@ant-design/icons';

//Formik
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';

//Icons
import { BiRocket } from "react-icons/bi";
import { InboxOutlined } from '@ant-design/icons';

// Custom components
import CustomTextArea from '../../UI/atoms/CustomTextArea';
import CustomButton from '../../UI/atoms/CustomButton';
import CustomInput from '../../UI/atoms/CustomInput';
import CustomBadge from '../../UI/atoms/CustomBadge';
import { config } from '../../../config/config';
import CustomTopInfo from '../../UI/molecules/CustomTopInfo';


const Aplication = () => {


  const { Panel } = Collapse;
  const { Dragger } = Upload;
  const [isLoading, setIsLoading] = useState(false);
  const [aplicationData, setAplicationData] = useState([]);
  const [formInitValues, setFormInitValue] = useState({
    id: '',
    app_file: '',
    app_version: '',
    app_description: '',
  });

  useEffect(() => {
    getAllAplication();
    setIsLoading(true);

  }, [])

  const props = {
    name: 'file',
    multiple: true,
    // action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    onChange(info) {
      const { status } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (status === 'done') {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      console.log('Dropped files', e.dataTransfer.files);
    },
  };

  //Get all aplication - AXIOS
  const getAllAplication = () => {
    return (
      axios.get(config.JSON_SERVER + 'aplication').then(function (response) {
        // handle success
        // console.log(response.data.reverse());
        setAplicationData(response.data);
        setTimeout(() => {
          setIsLoading(false);
        }, 500)

      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        })
    )
  }

  //Aplication validation schema
  const aplicationValidationSchema = Yup.object().shape({
    app_version: Yup.string()
      .max(10, 'Too Long!')
      .required('Obavezno polje!'),
  });


  // Page header
  const pageHeader = () => {
    return (
      <Row gutter={[24, 24]}>
        <Col span={12}>
          <h3 className='d-flex align-center'>Najnovija verzija aplikacije: <CustomBadge> <BiRocket /> 106</CustomBadge></h3>
          <h2 className='card-title'>Uploaduj novu verziju aplikacije</h2>
        </Col>
      </Row>
    )
  }

  // Upload form
  const formAplication = () => {
    return (
      <Formik
        initialValues={formInitValues}
        enableReinitialize={true}
        validationSchema={aplicationValidationSchema}
        onSubmit={(values, actions) => {

          setTimeout(() => {

            notification.open({
              message: 'Dodavanje nove verzije app',
              description:
                'Uspešno ste dodali novu verziju aplikacije.',
              icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
            });

            console.log(values);

            //Axios send data
            axios.post(config.JSON_SERVER + 'aplication',
              values
            ).then(function (response) {
              // handle success
              console.log(response);

            })
              .catch(function (error) {
                // handle error
                console.log(error);
              })
              .then(function () {
                // always executed
              })

          }, 1000);
        }}
      >
        {(props, handleSubmit) => (
          <Form>

            <Row gutter={[24, 12]}>
              <Col span={24}>
                <Dragger {...props}>
                  <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                  </p>
                  <p className="ant-upload-text">Click or drag file to this area to upload</p>
                  <p className="ant-upload-hint">
                    Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                    band files
                  </p>
                </Dragger>
              </Col>
              <Col span={24}>
                <Field name="app_version" type="number" label="Verzija aplikacije" component={CustomInput} />
                {props.errors.app_version && props.touched.app_version ? (
                  <div className="error-text">{props.errors.app_version}</div>
                ) : null}
              </Col>

              <Col span={24}>
                <Field name="app_description" label="Opis aplikacije (opciono)" component={CustomTextArea} />
              </Col>

              <Col span={24}>
                <CustomButton type="primary" htmlType="submit" className="login-form-button mt-2">Uploaduj</CustomButton>
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    )
  }

  // Description apk version
  const versionDescription = () => {
    return (
      <Collapse accordion defaultActiveKey={['1']}>
        {aplicationData.map((item, index) => {

          const { app_description, app_version, app_file, id } = item;
          return (index < 6) ? <Panel key={id} header={'Verzija ' + app_version}><p>{app_description}</p></Panel> : null
        })
        }
      </Collapse>
    )
  }


  return <>
    <CustomTopInfo>Aplikacija</CustomTopInfo>
    <section className='card-box'>
      {pageHeader()}

      <Row gutter={[24, 24]}>
        <Col span={12}>
          {formAplication()}
        </Col>
        <Col span={12}>
          <Skeleton active loading={isLoading} paragraph={{ rows: 6 }}>
            {versionDescription()}
          </Skeleton>
        </Col>
      </Row>
    </section>
  </>;
};

export default Aplication;
