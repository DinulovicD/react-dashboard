import React, { useEffect, useState, useMemo } from 'react';
import { useDropzone } from 'react-dropzone';
import axios from 'axios';


// Ant
import { Tabs, Card, Modal, Row, Col, Tooltip, Pagination, notification, Spin, Skeleton, Button } from 'antd';
import {
  CloudUploadOutlined,
  EditOutlined,
  DeleteOutlined,
  CheckCircleOutlined,
  QuestionCircleOutlined,
} from '@ant-design/icons';
import { BsUpload } from "react-icons/bs";
import { IoCloseOutline } from "react-icons/io5";
import { AiFillHtml5, AiOutlineFileZip } from "react-icons/ai";

//Formik
import { Formik, Form, Field } from 'formik';

//Lightgallery
import LightGallery from 'lightgallery/react';
import 'lightgallery/css/lightgallery.css';
import 'lightgallery/css/lg-zoom.css';
import 'lightgallery/css/lg-thumbnail.css';
import lgThumbnail from 'lightgallery/plugins/thumbnail';
import lgZoom from 'lightgallery/plugins/zoom';

//Custom 
import CustomBadge from '../../UI/atoms/CustomBadge';
import CustomTooltip from '../../UI/atoms/CustomTooltip';
import CustomPopConfirm from '../../UI/atoms/CustomPopConfirm';
import CustomImage from '../../UI/atoms/CustomImage';
import { config } from '../../../config/config';
import CustomInput from '../../UI/atoms/CustomInput';
import CustomButton from '../../UI/atoms/CustomButton';
import CustomTopInfo from '../../UI/molecules/CustomTopInfo';



const Plugins = () => {

  const { TabPane } = Tabs;
  const { Meta } = Card;

  //Use states
  const [pluginsData, setPluginsData] = useState([]);
  const [uploadImage, setUploadImage] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState({
    visible: false,
    currentItem: {}
  });
  const [formInitValues, setFormInitValue] = useState({
    name: '',
  });


  //Use effects
  useEffect(() => {
    getAllPlugins();

    setIsLoading(true);

  }, []);

  // Modal
  const showModal = (item) => {
    setIsModalVisible({
      visible: true,
      currentItem: item
    });
    setFormInitValue(item);
  };

  const handleOk = () => {
    setIsModalVisible({
      visible: false
    });
  };

  const handleCancel = () => {
    setIsModalVisible({
      visible: false
    });
  };

  //Get all plugins - AXIOS
  const getAllPlugins = () => {
    return (
      axios.get(config.JSON_SERVER + 'plugins').then(function (response) {
        // handle success
        console.log(response);
        setPluginsData(response.data);
        setTimeout(() => {
          setIsLoading(false);
        }, 500)

      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }

  //Delete plugin - AXIOS
  const deletePlugin = (id) => {
    return (
      axios.delete(config.JSON_SERVER + 'plugins/' + id).then(function (response) {
        // handle success
        console.log(response);
        getAllPlugins();

        notification.open({
          message: 'Brisanje plugina',
          description:
            'Uspešno ste obrisali plugin.',
          icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
        });


      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }

  //Confirmation button for delete user
  const deletePluginOk = (id) => {
    setTimeout(() => {
      <Spin />
      deletePlugin(id);
    }, 1000)
  }

  //Dropzone
  const remove = file => {
    const newFiles = [...files];     // make a var for the new array
    newFiles.splice(file, 1);        // remove the file from the array
    setFiles(newFiles);              // update the state
  };

  const focusedStyle = {
    borderColor: '#2196f3'
  };

  const acceptStyle = {
    borderColor: '#00e676'
  };

  const rejectStyle = {
    borderColor: '#ff1744'
  };

  const [files, setFiles] = useState([]);
  const [errors, setErrors] = useState("");
  const { getRootProps, getInputProps, isFocused, isDragAccept, isDragReject } = useDropzone({
    accept: '.zip',
    onDrop: (acceptedFiles, fileRejections) => {
      fileRejections.forEach((file) => {
        file.errors.forEach((err) => {
          console.log(err);
          if (err.code === "file-too-large") {
            // setErrors(`Error: ${err.message}`); 

            notification.open({
              message: 'Notification Title',
              description:
                err.message,
              onClick: () => {
                console.log('Notification Clicked!');
              },
            });
          }

          if (err.code === "file-invalid-type") {
            // setErrors(`Error: ${err.message}`); 

            notification.open({
              message: 'Notification Title',
              description:
                err.message,
              onClick: () => {
                console.log('Notification Clicked!');
              },
            });
          }
        });
      });

      setFiles(acceptedFiles.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      })));

    }
  });

  const style = useMemo(() => ({
    ...(isFocused ? focusedStyle : {}),
    ...(isDragAccept ? acceptStyle : {}),
    ...(isDragReject ? rejectStyle : {})
  }), [
    isFocused,
    isDragAccept,
    isDragReject
  ]);

  const thumbs = files.map((file, i) => (
    <div className='thumb' key={file.name}>
      <div className='thumb-inner'>
        <button type="button" onClick={() => remove(i)}><IoCloseOutline /></button>
        <img
          src={file.preview}
        />
      </div>
    </div>
  ));

  useEffect(() => {
    // Make sure to revoke the data uris to avoid memory leaks
    files.forEach(file => URL.revokeObjectURL(file.preview));
  }, [files]);


  const dropzoneWrapper = () => {
    return (
      <section className='dropzone-wrapper card-box d-block'>
        <div className="d-flex flex-wrap justify-end mb-3 py-4">

          <CustomBadge>
            <BsUpload />
            Max: 20MB
          </CustomBadge>

          <CustomBadge>
            <AiOutlineFileZip />
            ZIP
          </CustomBadge>

          <CustomBadge>
            <AiFillHtml5 />
            HTML
          </CustomBadge>

        </div>
        <div {...getRootProps({ style })} className="dropzone">
          <input {...getInputProps()} />
          <p> <CloudUploadOutlined /> Prevuci .zip fajl. Zip mora sadržati index.html na root nivou</p>
        </div>
        <aside className='thumbs-container'>
          {thumbs}
        </aside>
      </section>

    )
  }

  //Get all plugins - AXIOS
  const allPlugins = () => {
    return <>

      <Row gutter={[24, 12]} justify="start">

        {pluginsData.map((item, index) => {

          const { id, name, key, media_size, image } = item;

          return <>
            <Col span={6} key={id}>

              <Card
                className='card-custom mt-5'
                cover={
                  <LightGallery
                    speed={500}
                    plugins={[lgThumbnail, lgZoom]}
                  >
                    <a href={image} className="w-100">
                      <CustomImage src={image} alt={name} title={name} width="300" className="w-100" height="200" />
                    </a>

                  </LightGallery>
                }
                actions={[
                  <CustomTooltip title="Obriši" placement="bottom">
                    <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" onConfirm={() => deletePluginOk(id)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                      <DeleteOutlined key="delete" />
                    </CustomPopConfirm>
                  </CustomTooltip>,

                  <CustomTooltip title="Izmeni" placement="bottom">
                    <EditOutlined key="edit" onClick={() => showModal(item)} />
                  </CustomTooltip>,
                ]}
              >

                <Meta
                  title={name}
                  description={media_size}
                />

              </Card>

            </Col>

          </>

        })
        }

      </Row>
      <Row justify='center' className='mt-5'>
        <Col>
          <Pagination defaultCurrent={1} total={50} />
        </Col>
      </Row>
    </>


  }

  //Show modal detials
  const editModalPlugin = () => {
    return (
      <Modal centered title="Izmeni naziv" visible={isModalVisible.visible}
        footer={null}  onCancel={handleCancel}>

        <Formik
          initialValues={formInitValues}
          enableReinitialize={true}
          onSubmit={(values, actions) => {
            // console.log(values);

            axios.patch(config.JSON_SERVER + 'plugins/' + isModalVisible.currentItem?.id, values
            ).then(function (response) {
              // handle success
              console.log(response);

              notification.open({
                message: 'Izmena plugin-a',
                description:
                  'Uspešno ste izmenili naziv plugin-a.',
                icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
              });

              actions.setSubmitting(false);
              setIsModalVisible({
                visible: false
              });

              //Plugins
              getAllPlugins();


            }).catch(function (error) {
              // handle error
              console.log(error);
            })
          }}
        >
          {(props, handleSubmit, setFieldValue) => (
            <Form>
              <Row gutter={[24, 24]}>
                <Col span={24}>
                  <Field name="name" type="text" label="Naziv plugin-a" component={CustomInput} />
                </Col>
                <Col span={24} className="d-flex justify-end">
                  <CustomButton key="back" onClick={handleCancel}>
                    Odustani
                  </CustomButton>
                  <CustomButton key="submit" className="ml-3" htmlType="submit" type="primary">
                    Sačuvaj
                  </CustomButton>
                </Col>
              </Row>

            </Form>
          )}
        </Formik>

      </Modal>
    )
  }

  return <>

    <CustomTopInfo>Plugins</CustomTopInfo>

    {dropzoneWrapper()}

    <Skeleton active loading={isLoading} paragraph={{ rows: 6 }}>
      {allPlugins()}
    </Skeleton>

    {editModalPlugin()}

  </>;
};

export default Plugins;
