import React, { useEffect, useState, useMemo } from 'react';
import { useDropzone } from 'react-dropzone';

// Ant
import { Tabs, Popconfirm, Card, Modal, Row, Col, Tooltip, Pagination, notification, Button, Form } from 'antd';
import {
  FileImageOutlined,
  VideoCameraOutlined,
  CloudUploadOutlined,
  EditOutlined,
  ExclamationCircleOutlined,
  DeleteOutlined,
  QuestionCircleOutlined,
} from '@ant-design/icons';
import { BsUpload } from "react-icons/bs";
import { IoCloseOutline } from "react-icons/io5";

//Lightgallery
import LightGallery from 'lightgallery/react';
import 'lightgallery/css/lightgallery.css';
import 'lightgallery/css/lg-zoom.css';
import 'lightgallery/css/lg-thumbnail.css';
import lgThumbnail from 'lightgallery/plugins/thumbnail';
import lgZoom from 'lightgallery/plugins/zoom';

//Custom 
import CustomBadge from '../../UI/atoms/CustomBadge';
import CustomTooltip from '../../UI/atoms/CustomTooltip';
import CustomPopConfirm from '../../UI/atoms/CustomPopConfirm';
import CustomImage from '../../UI/atoms/CustomImage';
import { Field, Formik } from 'formik';
import CustomInput from '../../UI/atoms/CustomInput';
import { config } from '../../../config/config';
import axios from 'axios';
import CustomTopInfo from '../../UI/molecules/CustomTopInfo';


const Media = () => {


  const { TabPane } = Tabs;
  const { Meta } = Card;


  const [pluginsData, setPluginsData] = useState([]);
  const [uploadImage, setUploadImage] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [formInitValues, setFormInitValue] = useState({
    name: '',
  });

  const [state, setState] = useState({
    loading: true,
  });

  const onChange = checked => {
    setState({ loading: !checked });
  };

  const { loading } = state;

  // Modal
  const showModal = (data) => {
    setIsModalVisible(true);
    setFormInitValue(data);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  //Dropzone
  const remove = file => {
    const newFiles = [...files];     // make a var for the new array
    newFiles.splice(file, 1);        // remove the file from the array
    setFiles(newFiles);              // update the state
  };

  const focusedStyle = {
    borderColor: '#2196f3'
  };

  const acceptStyle = {
    borderColor: '#00e676'
  };

  const rejectStyle = {
    borderColor: '#ff1744'
  };

  const [files, setFiles] = useState([]);
  const [errors, setErrors] = useState("");
  const { getRootProps, getInputProps, isFocused, isDragAccept, isDragReject } = useDropzone({
    accept: 'image/*, video/*',
    maxSize: 2097152,
    onDrop: (acceptedFiles, fileRejections) => {
      fileRejections.forEach((file) => {
        file.errors.forEach((err) => {
          console.log(err);
          if (err.code === "file-too-large") {
            // setErrors(`Error: ${err.message}`); 

            notification.open({
              message: 'Fajl prevelik',
              description:
                err.message,
              icon: <ExclamationCircleOutlined style={{ color: '#d55252' }} />,
              onClick: () => {
                console.log('Notification Clicked!');
              },
            });
          }

          if (err.code === "file-invalid-type") {
            // setErrors(`Error: ${err.message}`); 

            notification.open({
              message: 'Pogrešan tip fajla',
              description: 'Vrsta fajla mora biti slika/*, video/*',
              icon: <ExclamationCircleOutlined style={{ color: '#d55252' }} />,
              onClick: () => {
                console.log('Notification Clicked!');
              },
            });
          }
        });
      });

      setFiles(acceptedFiles.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      })));

    }
  });

  const style = useMemo(() => ({
    ...(isFocused ? focusedStyle : {}),
    ...(isDragAccept ? acceptStyle : {}),
    ...(isDragReject ? rejectStyle : {})
  }), [
    isFocused,
    isDragAccept,
    isDragReject
  ]);

  const thumbs = files.map((file, i) => (
    <div className='thumb' key={file.name}>
      <div className='thumb-inner'>
        <button type="button" onClick={() => remove(i)}><IoCloseOutline /></button>
        <img
          src={file.preview}
        />
      </div>
    </div>
  ));

  useEffect(() => {
    // Make sure to revoke the data uris to avoid memory leaks
    files.forEach(file => URL.revokeObjectURL(file.preview));
  }, [files]);

  //Dropzone wrapper
  const dropzoneWrapper = () => {
    return (

      <section className='dropzone-wrapper card-box d-block'>
        <div className="d-flex flex-wrap justify-end mb-3 py-4">

          <CustomBadge>
            <BsUpload />
            Max: 2MB
          </CustomBadge>

          <CustomBadge>
            <VideoCameraOutlined />
            MP4
          </CustomBadge>

          <CustomBadge>
            <FileImageOutlined />
            PNG
          </CustomBadge>

          <CustomBadge>
            <FileImageOutlined />
            JPG
          </CustomBadge>

          <CustomBadge>
            <FileImageOutlined />
            JPEG
          </CustomBadge>

          <CustomBadge>
            <FileImageOutlined />
            SVG
          </CustomBadge>

          <CustomBadge>
            <FileImageOutlined />
            WEBP
          </CustomBadge>

        </div>
        <div {...getRootProps({ style })} className="dropzone">
          <input {...getInputProps()} />
          <p> <CloudUploadOutlined /> Prevuci slike ili video sadržaj</p>
        </div>
        <aside className='thumbs-container'>
          {thumbs}
        </aside>
      </section>
    )
  }


  return <>

    <CustomTopInfo>Media</CustomTopInfo>


    {dropzoneWrapper()}

    <div className="card-container mt-5">
      <Tabs type="card">

        <TabPane
          tab={
            <span>
              Sve
            </span>
          }
          key="1"
        >

          <Row gutter={[24, 24]} justify="start">

            <Col span={6}>
              <Card
                className="card-custom"
                cover={
                  <LightGallery
                    speed={500}
                    plugins={[lgThumbnail, lgZoom]}
                  >
                    <a href="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" className="w-100">
                      <CustomImage src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" className="w-100" alt="image" title="image" width="300" height="200" />
                    </a>
                  </LightGallery>
                }
                actions={[
                  <CustomTooltip title="Obriši" placement="bottom">
                    <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                      <DeleteOutlined key="delete" />
                    </CustomPopConfirm>
                  </CustomTooltip>,

                  <CustomTooltip title="Izmeni" placement="bottom">
                    <EditOutlined key="edit" onClick={showModal} />
                  </CustomTooltip>,
                ]}
              >

                <Meta
                  title="Video title"
                  description="1.2 MB"
                />

              </Card>
            </Col>

            <Col span={6}>
              <Card
                className="card-custom"
                cover={
                  <a href="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" className="w-100">
                    <CustomImage src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" className="w-100" alt="image" title="image" width="300" height="200" />
                  </a>
                }
                actions={[
                  <CustomTooltip title="Obriši" placement="bottom">
                    <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                      <DeleteOutlined key="delete" />
                    </CustomPopConfirm>
                  </CustomTooltip>,

                  <CustomTooltip title="Izmeni" placement="bottom">
                    <EditOutlined key="edit" onClick={showModal} />
                  </CustomTooltip>,
                ]}
              >

                <Meta
                  title="Video title"
                  description="1.2 MB"
                />

              </Card>
            </Col>

            <Col span={6}>
              <Card
                className="card-custom"
                cover={
                  <CustomImage src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" alt="image" title="image" width="300" height="200" />
                }
                actions={[
                  <CustomTooltip title="Obriši" placement="bottom">
                    <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                      <DeleteOutlined key="delete" />
                    </CustomPopConfirm>
                  </CustomTooltip>,

                  <CustomTooltip title="Izmeni" placement="bottom">
                    <EditOutlined key="edit" onClick={showModal} />
                  </CustomTooltip>,
                ]}
              >

                <Meta
                  title="Video title"
                  description="1.2 MB"
                />

              </Card>
            </Col>

            <Col span={6}>
              <Card
                className="card-custom"
                cover={
                  <CustomImage src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" alt="image" title="image" width="300" height="200" />
                }
                actions={[
                  <CustomTooltip title="Obriši" placement="bottom">
                    <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                      <DeleteOutlined key="delete" />
                    </CustomPopConfirm>
                  </CustomTooltip>,

                  <CustomTooltip title="Izmeni" placement="bottom">
                    <EditOutlined key="edit" onClick={showModal} />
                  </CustomTooltip>,
                ]}
              >

                <Meta
                  title="Video title"
                  description="1.2 MB"
                />

              </Card>
            </Col>

            <Col span={6}>
              <Card
                className="card-custom"
                cover={
                  <CustomImage src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png" alt="image" title="image" width="300" height="200" />
                }
                actions={[
                  <CustomTooltip title="Obriši" placement="bottom">
                    <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                      <DeleteOutlined key="delete" />
                    </CustomPopConfirm>
                  </CustomTooltip>,

                  <CustomTooltip title="Izmeni" placement="bottom">
                    <EditOutlined key="edit" onClick={showModal} />
                  </CustomTooltip>,
                ]}
              >

                <Meta
                  title="Video title"
                  description="1.2 MB"
                />

              </Card>
            </Col>

          </Row>

          <Row justify='center' className='mt-5'>
            <Col>
              <Pagination defaultCurrent={1} total={50} />
            </Col>
          </Row>

        </TabPane>


        <TabPane
          tab={
            <span>
              <FileImageOutlined />
              Slike
            </span>
          }
          key="3"
        >
          Slike
        </TabPane>

        <TabPane
          tab={
            <span>
              <VideoCameraOutlined />
              Video
            </span>
          }
          key="2"
        >
          Video
        </TabPane>

      </Tabs>
    </div>


    <Modal centered title="Izmeni naziv " onCancel={handleCancel} visible={isModalVisible} footer={[
      <Button key="back" onClick={handleCancel}>
        Odustani
      </Button>,
      <Button key="submit" type="primary" onClick={handleOk}>
        Sačuvaj
      </Button>,
    ]}>
      <Formik
        initialValues={formInitValues}
        enableReinitialize={true}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            console.log(JSON.stringify(values, null, 2));
            notification.open({
              message: 'Notification Title',
              description: JSON.stringify(values, null, 2),
            });


            axios.patch(config.JSON_SERVER + 'plugins/3', values
            ).then(function (response) {
              // handle success
              console.log(response);



            }).catch(function (error) {
              // handle error
              console.log(error);
            })

            actions.setSubmitting(false);
          }, 1000);
        }}
      >
        {(props, handleSubmit, setFieldValue) => (
          <Form>
            <Row gutter={[24, 24]}>
              <Col span={24}>
                <Field name="name" type="text" label="Naziv plugin-a" component={CustomInput} />
              </Col>
            </Row>

          </Form>
        )}
      </Formik>
    </Modal>

  </>;
};

export default Media;
