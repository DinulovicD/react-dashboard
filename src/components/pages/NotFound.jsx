import React, { useState } from 'react';
import { Link } from 'react-router-dom';

//Ant
import { Result, Button } from 'antd';


const NotFound = () => {

  return <>

    <Result
        status="404"
        title="404"
        subTitle="Stranica koju tražite ne postoji."
        extra={<Button type="primary"><Link to="/">Vratite se na Dashboard</Link></Button>}
    />

  </>;
};

export default NotFound;
