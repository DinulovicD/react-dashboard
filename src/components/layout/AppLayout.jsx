import React, { useContext, useEffect, useState } from "react";
import { Link, NavLink, Outlet, useLocation, useNavigate } from "react-router-dom";


// Antd 
import { Drawer, Dropdown, Space, Layout, Menu, Breadcrumb, Divider, Row, Col, Switch } from 'antd';
import { Footer } from 'antd/lib/layout/layout';

//Icons 
import {
  UserOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  SettingOutlined,
  UsbOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons';
import { BsMegaphone, BsBell, BsMusicNoteList } from "react-icons/bs";
import { MdOutlineBugReport } from "react-icons/md";
import { GrAppleAppStore } from 'react-icons/gr';
import { FiUsers, FiMonitor, FiLogOut } from 'react-icons/fi';
import { RiBuilding4Line, RiRoadMapLine, RiDashboard3Line, RiArrowGoBackFill, RiMoonLine } from 'react-icons/ri';
import { MdDevices } from 'react-icons/md';

//Context
import { UserContext } from "../../contexts/user.context";

//CSS
import '../../assets/scss/main.scss';

//Images
import LogoImage from './../../assets/images/logo.svg';
import LogoImageSM from './../../assets/images/logo-sm.svg';
import AvatarImage from './../../assets/images/deki.jpg';
import DarkLayout from './../../assets/images/dark.png';
import LightLayout from './../../assets/images/light.png';

//Components
import CustomImage from "../UI/atoms/CustomImage";
import DarkMode from '../../config/DarkMode';
import CustomSwitch from "../UI/atoms/CustomSwitch";




export const AppLayout = ({ children }) => {

  const navigate = useNavigate();
  const { logout, isAdmin, user } = useContext(UserContext);

  const signout = () => {
    logout();
    navigate("/login");
  };


  const location = useLocation();
  let href = window.location.href.split('/');
  href = href[3];

  const { Header, Sider, Content } = Layout;
  const { SubMenu } = Menu;

  //Use states
  const [state, setState] = useState({
    collapsed: false,
  });
  const [visible, setVisible] = useState(false);


  useEffect(() => {
    setState(localStorage.getItem("collapsed") === "true" ? true : false);
  }, []);

  const toggle = () => {
    // const headerClass = document.querySelector('.ant-layout-header');
    // headerClass.style.width = "calc(100% - 80px)";
    localStorage.setItem("collapsed", state.collapsed);
    setState({
      collapsed: !state.collapsed,
    });
  };

  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  //User options menu
  const userOptionsMenu = () => {
    return (
      <Menu>
        <Menu.Item key="1">
          <img className="avatar-image" src={AvatarImage}></img>
          <div className="info">
            <h3>{user.name}</h3>
            <p>{user.email}</p>
          </div>
        </Menu.Item>

        <Divider />

        <Menu.Item key="2">
          <Link to="/"> <UserOutlined /> Profil</Link>
        </Menu.Item>



        <Divider />

        <Menu.Item key="4">
          <div className="logout" to="/" onClick={signout}><FiLogOut /> Odjavi se</div>
        </Menu.Item>
      </Menu>
    )
  }

  //Logo header
  const logo = () => {
    return <>
      <div className="logo">
        {state.collapsed ?
          <img src={LogoImageSM} alt="OneView Logo" className="img-fluid" style={{ width: 70 }} />
          :
          <img src={LogoImage} alt="OneView Logo" className="img-fluid" />
        }
      </div>
    </>
  }

  //Header username + avatar
  const username = () => {
    return (
      <Space className="mr-4 d-flex align-center" size={10}>

        <Dropdown overlay={userOptionsMenu} trigger={['click']}>
          <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>

            <img className="avatar-image" src={AvatarImage}></img>
          </a>
        </Dropdown>
      </Space>
    )
  }

  //Sider
  const sider = () => {
    return (
      <Sider
        trigger={null}
        width={250}
        collapsible
        collapsed={state.collapsed}>
        {logo()}
        <Menu theme="dark" mode="inline" defaultSelectedKeys={['/' + href]} selectedKeys={['/' + href]}>
          <Menu.Item key="/" icon={<RiDashboard3Line />}>
            <NavLink to="/" >Dashboard</NavLink>
          </Menu.Item>
          <Menu.Item key="/media" icon={<VideoCameraOutlined />}>
            <NavLink to="/media" >Media</NavLink>
          </Menu.Item>
          <Menu.Item key="/plugins" icon={<UsbOutlined />}>
            <NavLink to="/plugins" >Pluginovi</NavLink>
          </Menu.Item>
          <Menu.Item key="/playlist" icon={<BsMusicNoteList />}>
            <NavLink to="/playlist" >Playliste</NavLink>
          </Menu.Item>
          <SubMenu key="1" icon={<FiMonitor />} title="Uređaji">
            <Menu.Item key="/devices" icon={<FiMonitor />}><NavLink to="/devices" >Lista</NavLink></Menu.Item>
            <Menu.Item key="/group-devices" icon={<MdDevices />}><NavLink to="/group-devices" >Grupe</NavLink></Menu.Item>
            <Menu.Item key="/map-devices" icon={<RiRoadMapLine />}><NavLink to="/map-devices" >Mapa</NavLink></Menu.Item>
          </SubMenu>
          <Menu.Item key="/companies" icon={<RiBuilding4Line />}>
            <NavLink to="/companies" >Kompanije</NavLink>
          </Menu.Item>
          <Menu.Item key="/users" icon={<FiUsers />}>
            <NavLink to="/users" >Korisnici</NavLink>
          </Menu.Item>
          <Menu.Item key="/chyrons" icon={<BsBell />}>
            <NavLink to="/chyrons" >Kajroni</NavLink>
          </Menu.Item>
          <Menu.Item key="/aplication" icon={<GrAppleAppStore />}>
            <NavLink to="/aplication" >Aplikacija</NavLink>
          </Menu.Item>
          <Menu.Item key="/advertiser" icon={<BsMegaphone />}>
            <NavLink to="/advertiser" >Reklamiranje</NavLink>
          </Menu.Item>
          <Menu.Item key="/reports" icon={<MdOutlineBugReport />}>
            <NavLink to="/reports" >Izveštaji</NavLink>
          </Menu.Item>
        </Menu>
      </Sider>
    )
  }


  const onChangeLightLayout = () => {
    console.log('change it on dark mode');
  }

  const onChangeDarkLayout = () => {
    console.log('change it on light mode');
  }

  // App settings
  const appSettings = () => {
    return <>
      <Drawer title="Podešavanja" placement="right" onClose={onClose} visible={visible}>
        <div className="item mb-10">
          <h3 className="card-title">Izaberite layout</h3>
          <div className="light-layout mb-7">
            <CustomImage src={LightLayout}></CustomImage>
            <CustomSwitch defaultChecked onChange={onChangeLightLayout}>Light layout</CustomSwitch>
          </div>
 
          <div className="dark-layout mb-5">
            <CustomImage src={DarkLayout}></CustomImage>
            <CustomSwitch onChange={onChangeDarkLayout}>Dark layout</CustomSwitch>
          </div>
        </div>

        <div className="item mb-10">
          <h3 className="card-title">Uključite notifikaciju</h3>
          <CustomSwitch onChange={onChangeDarkLayout}>Uključi</CustomSwitch>

        </div>

        <div className="item mb-10">
          <h3 className="card-title">Uključite lokaciju</h3>
          <CustomSwitch onChange={onChangeDarkLayout}>Uključi</CustomSwitch>

        </div>
      </Drawer>
    </>
  }

  return (

    <Layout>
      {sider()}
      {appSettings()}

      <Layout className="site-layout" id='site-layout' style={state.collapsed ? { marginLeft: 80 } : { marginLeft: 250 }} >
        <Header className="site-layout-background" style={state.collapsed ? { width: 'calc(100% - 80px)' } : { width: 'calc(100% - 250px)' }}>
          {React.createElement(state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: toggle,
          })}
          <div className="d-flex align-center">
            {user.name}
            {username()}
            <div className="app-settings">
              <SettingOutlined onClick={showDrawer} />
            </div>
          </div>
        </Header>
        <Content
          className={href}
          style={{
            // background: '#fff',
            margin: '24px 16px',
            padding: 24,
            minHeight: 280,
          }}
        >

          {children}
          <Outlet />
        </Content>
        <Footer>
          <Row gutter={[24, 24]} >
            <Col span={12}>2022 © OneView.</Col>
            <Col span={12} className="d-flex justify-end">Design & Develop by IT Centar</Col>
          </Row>
        </Footer>
      </Layout>
    </Layout>

  );
}
