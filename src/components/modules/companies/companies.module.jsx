import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { AllCompanies } from "./pages/all-companies.page";
import { AddCompany } from "./pages/add-company.page";
import { EditCompany } from "./pages/edit-company.page";

export const Companies = () => { 
  return (
    <Routes>  
      <Route path="/">
        <Route path="/" element={<AllCompanies />} />
        <Route path="/create" element={<AddCompany />} />
        <Route path="/edit/:id" element={<EditCompany />} />
      </Route>
    </Routes>
  );
};
