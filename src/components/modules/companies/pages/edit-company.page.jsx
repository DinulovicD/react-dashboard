import React, { useEffect, useRef, useState } from 'react';

//Ant
import { Button, Select, Col, notification, PageHeader, Row, Spin } from 'antd';

//Icons
import { BsSave } from "react-icons/bs";

//Formik
import { Formik, Form, Field } from 'formik';

// Custom components
import CustomInput from '../../../UI/atoms/CustomInput';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';

export const EditCompany = () => {

  const { Option } = Select;

  //Use states
  const [value, setValue] = useState('');
  const [formInitValues, setFromInitValues] = useState({
    name: 'MAX',
    email: '',
    password: '',
    password_confirmation: '',
    phone: '',
    role_id: '',
  });

  const submitForm = useRef(null);

  const header = () => {
    return (
      <>
        <PageHeader
          className="pl-0 pr-0"
          ghost={false}
          onBack={() => window.history.back()}
          title="Nazad"
          extra={[
            <Button
              key="1"
              type="primary"
            >
              Sačuvaj
              <BsSave />
            </Button>,
          ]}
        ></PageHeader>
      </>
    );
  };

  const editDeviceForm = () => {
    return (
      <Formik
        initialValues={{
          app_file: '',
          app_version: '',
          app_description: '',
        }}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            console.log(JSON.stringify(values, null, 2));
            notification.open({
              message: 'Notification Title',
              description: JSON.stringify(values, null, 2),
            });
            actions.setSubmitting(false);
          }, 1000);
        }}
      >
        {(props, handleSubmit) => (
          <Form>
            <Row gutter={[24, 24]} className="mb-5">
              <Col span={24}><h2 className='card-title mb-0'>Podaci kompanije</h2></Col>
              <Col span={12}>
                <Field name="company_name" type="text" label="Naziv kompanije" component={CustomInput} />
              </Col>

              <Col span={12}>
                <Field name="space" type="number" label="Prostor (u MB)" component={CustomInput} />
              </Col>

              <Col span={12}>
                <Field name="licence_start" type="date" label="Licenca od" component={CustomInput} />
              </Col>

              <Col span={12}>
                <Field name="licence_end" type="date" label="Do" component={CustomInput} />
              </Col>

              <Col span={12}>
                <Field name="widget_font_color" type="color" label="Widgeti - boja teksta" component={CustomInput} />
              </Col>

              <Col span={12}>
                <Field name="widget_bg_color" type="color" label="Widgeti - boja pozadine" component={CustomInput} />
              </Col>

            </Row>

            <Row gutter={[24, 24]}>
              <Col span={24}><h2 className='card-title mb-0 mt-5'>Podaci administratora</h2></Col>
              <Col span={8}>
                <Field name="name" type="text" label="Ime i prezime" component={CustomInput} />
              </Col>

              <Col span={8}>
                <Field name="phone" type="tel" label="Kontakt telefon (opciono)" component={CustomInput} />
              </Col>

              <Col span={8}>
                <Field name="email" type="email" label="Email adresa" component={CustomInput} />
              </Col>

              <Col span={12}>
                <Field name="password" type="password" label="Lozinka" component={CustomInput} />
              </Col>

              <Col span={12}>
                <Field name="password_again" type="password" label="Potvrda lozinke" component={CustomInput} />
              </Col>


            </Row>

          </Form>
        )}
      </Formik>
    );
  };

  return (
    <>
      <CustomTopInfo>Izmena kompanije</CustomTopInfo>

      <section className='card-box'>
        {header()}
        {editDeviceForm()}
      </section>
    </>
  );
};
