import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import axios from 'axios';

//Antd
import { Table, Icon, Select, Row, Col, Input, Button, Space, Popconfirm, notification, Spin } from 'antd';

//Icons
import {
  ReloadOutlined,
  DeleteOutlined,
  CheckCircleOutlined,
  EditOutlined,
  MailOutlined,
  QuestionCircleOutlined
} from '@ant-design/icons';
import { AiOutlinePlus } from "react-icons/ai";
import { IoCheckmarkOutline } from "react-icons/io5";
import { IoMdClose } from "react-icons/io";
import { MdOutlineAdminPanelSettings } from "react-icons/md";

//Custom components
import CustomSearchInput from '../../../UI/atoms/CustomSearchInput';
import CustomTooltip from '../../../UI/atoms/CustomTooltip';
import CustomPopConfirm from '../../../UI/atoms/CustomPopConfirm';
import { config } from '../../../../config/config';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';

export const AllCompanies = () => {

  const { Option } = Select;
  const { Search } = Input;

  // Use states
  const [data, setData] = useState([]);
  const [filters, setFilters] = useState({});
  const [role, setRole] = useState([]);
  const [options, setOptions] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  //Use effects 
  useEffect(() => {

    //Companies
    getAllCompanies();
    setIsLoading(true);

  }, [filters]);

  //Get all companies - AXIOS
  const getAllCompanies = () => {
    return (
      axios.get(config.JSON_SERVER + 'companies', { params: filters }).then(function (response) {
        // handle success
        // console.log(response);
        setData(response.data.map(v => { return { ...v, key: v.id } }));
        setTimeout(() => {
          setIsLoading(false);
        }, 500)


      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }

  // Delete companies by id - AXIOS
  const deleteCompany = (id) => {
    return (
      axios.delete(config.JSON_SERVER + 'companies/' + id).then(function (response) {
        // handle success
        console.log(response);
        getAllCompanies();

        notification.open({
          message: 'Brisanje kompanije',
          description:
            'Uspešno ste obrisali kompaniju.',
          icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
        });


      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }

  //Get role - AXIOS
  const getAllRole = () => {
    return (
      axios.get(config.JSON_SERVER + 'role').then(function (response) {
        // handle success
        // console.log(response);
        setRole(response.data);

        setOptions(response.data.map(v => {

          return ({
            label: v.name,
            value: v.name
          });
        }));


      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }

  //Confirmation button for delete company
  const deleteCompanyOk = (id) => {
    setTimeout(() => {
      <Spin />
      deleteCompany(id);
    }, 1000)
  }



  //Search email address
  const onSearchCompanyName = (value) => {
    setFilters({
      ...filters,
      name_like: value
    })
  }

  //On change company filter
  const onChangeRoleFilter = (value) => {

    setFilters({
      ...filters,
      role_like: value
    });

  }

  const resetFilters = () => {
    setFilters({});
  }


  /*
* Main content
*/

  const columns = [
    {
      title: 'Info',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Prostor',
      dataIndex: 'space',
      key: 'space',
    },
    {
      title: 'uređaji',
      dataIndex: 'devices',
      key: 'devices',
    },
    {
      title: 'Korisnici',
      dataIndex: 'users',
      key: 'users',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (data) => (
        data ? <IoCheckmarkOutline /> : <IoMdClose />
      )
    },
    {
      title: 'Licenca do',
      dataIndex: 'licence',
      key: 'licence',
    },
    {
      title: 'Akcije',
      key: 'akcije',
      width: '10%',
      render: (data) => (
        <>
          <Space size="middle">
            <CustomTooltip title="Administriraj"><Button className="mr-2 d-flex align-center justify-center" icon={<MdOutlineAdminPanelSettings />} /></CustomTooltip>

            <CustomTooltip title="Izmeni"><Link to={`/companies/edit/${data.id}`}><Button className="mr-2 d-flex align-center justify-center" icon={<EditOutlined />} /></Link></CustomTooltip>

            <CustomTooltip title="Obriši">
              <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" onConfirm={() => deleteCompanyOk(data.id)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                <Button className="mr-2 d-flex align-center justify-center" icon={<DeleteOutlined />} />
              </CustomPopConfirm>
            </CustomTooltip>
          </Space>
        </>
      )
    },
  ];


  const companyFilters = () => {
    return (
      <Row gutter={[24, 24]} className='mb-4' justify='end'>
        <Col flex="auto" className='d-flex'>
          <Link to="/company/add"><Button type="primary" className='d-flex align-center mr-3'> Dodaj kompaniju <AiOutlinePlus /></Button></Link>
          <Link><Button type="primary" className='d-flex align-center'> Pošalji mejlove za licencu <MailOutlined /></Button></Link>
        </Col>
        <Col span={5}><CustomSearchInput placeholder="Pretraži po nazivu" onSearch={onSearchCompanyName} /></Col>
        <Col><Button icon={<ReloadOutlined />} onClick={() => resetFilters()} /></Col>
      </Row>
    )
  }

  const companyTable = () => {
    return (
      <Table rowKey={(data) => data.id} loading={isLoading} columns={columns} bordered dataSource={data} />
    )
  }


  return <>
    <CustomTopInfo>Kompanije</CustomTopInfo>

    <section className='card-box'>
      {companyFilters()}

      {companyTable()}
    </section>
  </>;
};
