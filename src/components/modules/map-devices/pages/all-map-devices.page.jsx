import React, { useEffect, useRef, useState } from 'react';
import { Col, Row, Button, Select } from 'antd';
import {
  ReloadOutlined,
  DeleteOutlined,
  EditOutlined,
  QuestionCircleOutlined
} from '@ant-design/icons';
import CustomSelect from '../../../UI/atoms/CustomSelect';
import CustomSearchInput from '../../../UI/atoms/CustomSearchInput';
import CustomMapDevices from '../../../UI/atoms/CustomMapDevices';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';

export const AllMapDevices = () => {


  const { Option } = Select;
  const onSearch = value => console.log(value);

  //Use states
  const [filters, setFilters] = useState({});


  const resetFilters = () => {
    setFilters({});
  }

  const mapFilters = () => {
    return (
      <Row gutter={[24, 24]} className='mb-4' justify='end'>
        <Col flex="auto">
          <CustomSelect
            style={{ width: '100%' }}
            showSearch
            placeholder="Izaberite lokaciju"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            } >
            <Option value="Jack">Jack</Option>
            <Option value="Dzon">Dzon</Option>
          </CustomSelect>
        </Col>
        <Col flex="auto">
          <CustomSelect
            style={{ width: '100%' }}
            showSearch
            placeholder="Izaberite grupu"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            } >
            <Option value="Jack">Jack</Option>
            <Option value="Dzon">Dzon</Option>
          </CustomSelect>
        </Col>
        <Col flex="auto">
          <CustomSelect
            style={{ width: '100%' }}
            showSearch
            placeholder="Izaberite tip uređaja"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            } >
            <Option value="Jack">Jack</Option>
            <Option value="Dzon">Dzon</Option>
          </CustomSelect>
        </Col>
        <Col flex="auto"><CustomSearchInput placeholder="Pretraži email adrese" onSearch={onSearch} /></Col>
        <Col><Button icon={<ReloadOutlined />} onClick={() => resetFilters()} /></Col>
      </Row>
    )
  }

  const mapDevices = () => {
    return (
      <CustomMapDevices></CustomMapDevices>
    )
  }

  const listDevices = () => {
    return (
      <div className="list-device">
        <div className="list-address">
          <h4>Box1</h4>
          <p className="my-1">Adresa: <strong>Cara Dušana, Novi Sad, Serbia </strong></p>
          <p className="my-1">Tip uređaja: <strong>Box</strong></p>
          <p className="my-1">Grupa uređaja: <strong>ZaDjoletaTest</strong></p>
        </div>

        <div className="list-address">
          <h4>Box1</h4>
          <p className="my-1">Adresa: <strong>Cara Dušana, Novi Sad, Serbia </strong></p>
          <p className="my-1">Tip uređaja: <strong>Box</strong></p>
          <p className="my-1">Grupa uređaja: <strong>ZaDjoletaTest</strong></p>
        </div>

        <div className="list-address">
          <h4>Box1</h4>
          <p className="my-1">Adresa: <strong>Cara Dušana, Novi Sad, Serbia </strong></p>
          <p className="my-1">Tip uređaja: <strong>Box</strong></p>
          <p className="my-1">Grupa uređaja: <strong>ZaDjoletaTest</strong></p>
        </div>

        <div className="list-address">
          <h4>Box1</h4>
          <p className="my-1">Adresa: <strong>Cara Dušana, Novi Sad, Serbia </strong></p>
          <p className="my-1">Tip uređaja: <strong>Box</strong></p>
          <p className="my-1">Grupa uređaja: <strong>ZaDjoletaTest</strong></p>
        </div>

        <div className="list-address">
          <h4>Box1</h4>
          <p className="my-1">Adresa: <strong>Cara Dušana, Novi Sad, Serbia </strong></p>
          <p className="my-1">Tip uređaja: <strong>Box</strong></p>
          <p className="my-1">Grupa uređaja: <strong>ZaDjoletaTest</strong></p>
        </div>
      </div>
    )
  }


  return <>
        <CustomTopInfo>Mapa uređaj</CustomTopInfo>

    <section className='card-box'>
      {mapFilters()}
      <Row gutter={[24, 24]}>
        <Col span={18}>
          {mapDevices()}
        </Col>
        <Col span={6}>
          {listDevices()}
        </Col>
      </Row>
    </section>
  </>;

};

