import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { AllMapDevices } from "./pages/all-map-devices.page"; 

export const MapDevices = () => { 
  return (
    <Routes> 
      <Route path="/">
        <Route path="/" element={<AllMapDevices />} />
      </Route>
    </Routes>
  );
};
