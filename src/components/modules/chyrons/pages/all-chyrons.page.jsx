import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

//Antd
import { Table, Select, Row, Col, Input, Button, Space, Modal, notification, Card, Popconfirm, Spin, Skeleton } from 'antd';

//Icons
import {
  ReloadOutlined,
  QuestionCircleOutlined,
  InfoCircleOutlined,
  DeleteOutlined,
  CheckCircleOutlined,
  EyeOutlined,
  EditOutlined,
} from '@ant-design/icons';
import { BsSave, BsEye } from "react-icons/bs";
import { AiOutlineInfoCircle } from "react-icons/ai";
import { FiMonitor } from 'react-icons/fi';
import { MdDevices } from 'react-icons/md';

// Custom components
import CustomSearchInput from '../../../UI/atoms/CustomSearchInput';
import axios from 'axios';
import { config } from '../../../../config/config';
import { Formik, Form } from 'formik';
import Meta from 'antd/lib/card/Meta';
import CustomTooltip from '../../../UI/atoms/CustomTooltip';
import CustomPopConfirm from '../../../UI/atoms/CustomPopConfirm';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';

export const AllChyrons = () => {

  const { Option } = Select;
  const { Search } = Input;

  //Use states
  const [chyronsData, setChyronsData] = useState([]);
  const [filters, setFilters] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [isModalPreview, setIsModalPreview] = useState({
    visible: false,
    currentItem: {}
  });
  const [isModalVisible, setIsModalVisible] = useState({
    visible: false,
    currentItem: {}
  });

  //Use effects
  useEffect(() => {

    //Chyrons
    getAllChyrons();
    setIsLoading(true);

  }, []);

  //Modal
  const showModal = (item) => {
    setIsModalVisible({
      visible: true,
      currentItem: item
    });
  };

  const clickModalPreview = (item) => {
    setIsModalPreview({
      visible: true,
      currentItem: item
    });
  };

  const handleOk = () => {
    setIsModalVisible({
      visible: false
    });
  };

  const handleCancel = () => {
    setIsModalVisible({
      visible: false
    });
    setIsModalPreview({ 
      visible: false
    });
  };



  //Header
  const pageHeader = () => {
    return (
      <Row gutter={[24, 24]} className='mb-4' justify='between'>
        <Col span={24} className="d-flex justify-end ">
          <Link to="/chyrons/edit-many-devices"><Button type="primary" className='mr-3 d-flex align-center'>Izmeni više uređaja <EditOutlined className='ml-0' /></Button></Link>
          <Link to="/chyrons/edit-many-group-devices"><Button type="primary" className='d-flex align-center'>Izmeni više grupa <EditOutlined className='ml-0' /></Button></Link>
        </Col>
      </Row>
    )
  }

  //Filters
  const filterChrons = () => {
    return (
      <Row gutter={[24, 24]} className='mb-4' justify='end'>

        <Col span={4}>
          <CustomSearchInput placeholder="Pretraži uređaje po imenu" />
        </Col>
        <Col>
          <Button icon={<ReloadOutlined />} />
        </Col>
      </Row>
    )
  }

  //Get all chyron - AXIOS
  const getAllChyrons = () => {
    return (
      axios.get(config.JSON_SERVER + 'chyrons').then(function (response) {
        // handle successt
        // console.log(response);
        setChyronsData(response.data)
        setTimeout(() => {
          setIsLoading(false);
        }, 500)

      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        })
    )
  }

  // Delete user by id - AXIOS
  const deleteChyron = (id) => {
    return (
      axios.delete(config.JSON_SERVER + 'chyrons/' + id).then(function (response) {
        // handle success
        console.log(response);
        getAllChyrons();

        notification.open({
          message: 'Brisanje kajrona',
          description:
            'Uspešno ste obrisali kajrona.',
          icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
        });


      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }

  //Confirmation button for chryron user
  const deleteChyronBtn = (id) => {
    setTimeout(() => {
      <Spin />
      deleteChyron(id);
    }, 1000)
  }


  //Show chryrons
  const showAllChyrons = () => {
    return <>
      {chyronsData.map((item, index) => {
        const { id, device, message, font_color, bg_color, font_size } = item;

        return <>
          <Col span={8} key={index}>
            <Card
              key={index}
              className='card-custom'
              style={{ marginTop: 16 }}
              actions={[
                <CustomTooltip title="Pogledaj preview">
                  <EyeOutlined onClick={() => clickModalPreview(item)} id={id} key="setting" />
                </CustomTooltip>,

                <CustomTooltip title="Informacije">
                  <InfoCircleOutlined onClick={() => showModal(item)} key="ellipsis" />
                </CustomTooltip>,

                <CustomTooltip title="Izmeni">
                  <Link to={`/chyrons/edit/${id}`}> <EditOutlined key="edit" /></Link>
                </CustomTooltip>,

                <CustomTooltip title="Obriši">
                  <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" onConfirm={() => deleteChyronBtn(id)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                    <DeleteOutlined key="ellipsis" />
                  </CustomPopConfirm>
                </CustomTooltip>,
              ]}
            >
              <Meta

                title={device}
                description={message.slice(0, 200) + '...'}
              />
            </Card>
          </Col>



        </>
      })}
    </>
  }

  //Show modal preview
  const showModalPreview = () => {
    return (
      <Modal
        title={"Pregled izgleda kajrona - " + isModalPreview.currentItem?.device}
        centered
        visible={isModalPreview.visible}
        className="chyrons-modal"
        onCancel={() => setIsModalPreview(false)}
        width={1000}
        footer={null}
      >
        <div className="ticker-wrap" style={{ backgroundColor: isModalPreview.currentItem?.bg_color }}>
          <div className="ticker" id="ticker" >
            <div className="ticker-item" style={{ fontSize: isModalPreview.currentItem?.font_size + 'px', color: isModalPreview.currentItem?.font_color }}>
              {isModalPreview.currentItem?.message}
            </div>
          </div>
        </div>
      </Modal>
    )
  }

  //Show modal detials
  const showModalDetails = () => {
    return (
      <Modal centered title="Informacije uređaj" visible={isModalVisible.visible} okText="Sačuvaj"
      cancelText="Odustani" onOk={handleOk} onCancel={handleCancel}>
        <ul>
          <li><strong>Naziv uređaja: </strong> {isModalVisible.currentItem?.device}</li>
          <li><strong>Tip uređaja: </strong> Box</li>
          <li><strong>Lokacija: </strong> Cara Dušana, Novi Sad, Serbia</li>
          <li><strong>Trenutna grupa: </strong> ZaDjoletaTest</li>
        </ul>
      </Modal>
    )
  }

  

  return <>
    <CustomTopInfo>Kajroni</CustomTopInfo>

    {pageHeader()}

    {/* {filterChrons()} */}

    <Skeleton active loading={isLoading} paragraph={{ rows: 6 }}>
      <Row gutter={[24, 12]}>
        {showAllChyrons()}
      </Row>
    </Skeleton>

    {showModalPreview()}
    {showModalDetails()}
  </>;
};

