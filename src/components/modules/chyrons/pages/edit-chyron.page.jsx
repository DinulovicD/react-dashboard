import React, { useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';

//Antd
import { Button, Select, Col, notification, PageHeader, Row, Modal, Skeleton } from 'antd';

//Formik
import { Formik, Form, Field } from 'formik';

//Icons
import {
    CheckCircleOutlined,
} from '@ant-design/icons';
import { BsSave, BsEye } from "react-icons/bs";

//Custom components
import CustomInput from '../../../UI/atoms/CustomInput';
import { config } from '../../../../config/config';
import CustomTextArea from '../../../UI/atoms/CustomTextArea';
import { Redirect } from 'react-router-dom';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';

export const EditChyron = () => {

    const { id: chyronID } = useParams();


    //Use states
    const [chyronData, setChyronData] = useState({});
    const [visible, setVisible] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [formInitValues, setFormInitValue] = useState({
        message: '',
        font_color: '',
        bg_color: '',
        font_size: '',
    });


    //Use effects
    useEffect(() => {
        getOne(chyronID);
        setIsLoading(true);

        setFormInitValue({
            message: chyronData.message,
            font_color: chyronData.font_color,
            bg_color: chyronData.bg_color,
            font_size: chyronData.font_size,
        })

    }, []);


    // AXIOS get one chyron
    const getOne = (id) => {
        return (
            axios.get(config.JSON_SERVER + 'chyrons/' + id).then(function (response) {

                setFormInitValue(response.data);
                setChyronData(response.data);
                setTimeout(() => {
                    setIsLoading(false);
                }, 500)

            })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
        )
    }

    //Page header
    const pageHeader = () => {
        return (
            <>
                <PageHeader
                    className="pl-0 pr-0"
                    ghost={false}
                    onBack={() => window.history.back()}
                    title="Nazad"
                    extra={[
                        <Button
                            key="1"
                            onClick={() => setVisible(true)}
                            type="primary"
                        >
                            Pregled
                            <BsEye />
                        </Button>,
                        <Button
                            key="2"
                            htmlType="submit"
                            type="primary"
                        >
                            Sačuvaj
                            <BsSave />
                        </Button>,
                    ]}
                ></PageHeader>
            </>
        );
    };

    //Edit chyron form
    const editChyronForm = () => {
        return (
            <Formik
                initialValues={formInitValues}
                enableReinitialize={true}
                onSubmit={(values, actions) => {
                    // Axios send data
                    axios.patch(config.JSON_SERVER + 'chyrons/' + chyronID, values
                    ).then(function (response) {
                        // handle success
                        console.log(response);

                        notification.open({
                            message: 'Izmena kajrona',
                            description:
                                'Uspešno ste izmenili podatke.',
                            icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
                        });

                    }).catch(function (error) {
                        // handle error
                        console.log(error);
                    })

                    actions.setSubmitting(false);
                }}
            >
                {(props, handleSubmit, setFieldValue) => (
                    <Form>
                        {pageHeader()}
                        <Row gutter={[24, 24]}>
                            <Col span={24}>
                                <Field name="message" type="text" onChange={handleChyronSettings} label="Poruka (Ispraznite polje i sačuvajte da deaktivirate kajron)" component={CustomTextArea} />
                            </Col>

                            <Col span={8}>
                                <Field name="font_color" type="color" onChange={handleChyronSettings} label="Boja teksta" component={CustomInput} />
                            </Col>

                            <Col span={8}>
                                <Field name="bg_color" type="color" onChange={handleChyronSettings} label="Boja pozadine" component={CustomInput} />
                            </Col>

                            <Col span={8}>
                                <Field name="font_size" type="text" onChange={handleChyronSettings} label="Veličina teksta" component={CustomInput} />
                            </Col>
                        </Row>

                    </Form>
                )}
            </Formik>
        );
    };

    const handleChyronSettings = (e) => {
        console.log(e.target.name);

        setFormInitValue({
            ...formInitValues,
            [e.target.name]: e.target.value
        })



    }


    // Chyron modal preview
    const modalPreviewChyron = () => {

        // console.log(chyronStyle); 
        return <>

            <Modal
                title="Pregled izgleda kajrona"
                centered
                visible={visible}
                className="chyrons-modal"
                onCancel={() => setVisible(false)}
                width={1000}
                footer={null}
            >
                <div className="ticker-wrap" style={{ backgroundColor: formInitValues.bg_color }}>
                    <div className="ticker" id="ticker" >
                        <div className="ticker-item" style={{ fontSize: formInitValues.font_size + 'px', color: formInitValues.font_color }}>
                            {formInitValues.message}
                        </div>
                    </div>
                </div>
            </Modal>
        </>
    }

    return <>
        <Skeleton active loading={isLoading} paragraph={{ rows: 6 }}>
            <CustomTopInfo>Izmena kajrona - {chyronData.device}</CustomTopInfo>
            <section className='card-box'>
                {editChyronForm()}
            </section>
        </Skeleton>

        {modalPreviewChyron()}
    </>;
}
