import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { AllChyrons } from "./pages/all-chyrons.page";
import { EditChyron } from "./pages/edit-chyron.page";
import { EditManyDeviceChyrons } from "./pages/edit-many-device-chyrons.page";
import { EditManyGroupChyrons } from "./pages/edit-many-group-chyrons.page";

export const Chyrons = () => { 
  return (
    <Routes>  
      <Route path="/">
        <Route path="/" element={<AllChyrons />} />
        <Route path="/edit/:id" element={<EditChyron />} />
        <Route path="/edit-many-devices" element={<EditManyDeviceChyrons />} />
        <Route path="/edit-many-group-devices" element={<EditManyGroupChyrons />} />
      </Route>
    </Routes>
  );
};
