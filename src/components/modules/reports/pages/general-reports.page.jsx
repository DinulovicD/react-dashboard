import React, { useState } from 'react'; 

//Antd
import { Table, DatePicker, Select, Row, Col, Button, Space, PageHeader, Tag, } from 'antd';


//Icons
import {
    ReloadOutlined,
    CheckOutlined
} from '@ant-design/icons';

//Custom components
import CustomSearchInput from '../../../UI/atoms/CustomSearchInput';
import CustomSelect from '../../../UI/atoms/CustomSelect';
import CustomTooltip from '../../../UI/atoms/CustomTooltip';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';

export const GeneralReports = () => {

    const { Option } = Select;
    const { RangePicker } = DatePicker;

    // Use states
    const [isLoading, setIsLoading] = useState(false);


    const pageHeader = () => {
        return (
            <>
                <PageHeader
                    className="pl-0 pr-0"
                    ghost={false}
                    onBack={() => window.history.back()}
                    title="Nazad"
                    extra={[
                        <Button
                            key="1"
                            htmlType="submit"
                            type="primary"
                        >
                            Reši sve probleme
                            <CheckOutlined />
                        </Button>,
                    ]}
                ></PageHeader>
            </>
        );
    };

    // Reports filters
    const reportFilters = () => {
        return (
            <Row gutter={[24, 24]} className='mb-4' justify='start'>

                <Col flex="auto">
                    <CustomSelect
                        style={{ width: '100%' }}
                        showSearch
                        placeholder="Izaberite tip"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                        filterSort={(optionA, optionB) =>
                            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                        } >
                        <Option value="Jack">Jack</Option>
                        <Option value="Dzon">Dzon</Option>
                    </CustomSelect>
                </Col>
                <Col flex="auto"><CustomSearchInput placeholder="Pretraži po opisu" /></Col>
                <Col flex="auto">
                    <CustomSelect
                        style={{ width: '100%' }}
                        showSearch
                        placeholder="Izaberite status"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                        filterSort={(optionA, optionB) =>
                            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                        } >
                        <Option value="Jack">Jack</Option>
                        <Option value="Dzon">Dzon</Option>
                    </CustomSelect>
                </Col>
                <Col flex="auto"><RangePicker style={{ width: '100%' }} /></Col>
                <Col><Button icon={<ReloadOutlined />} /></Col>
            </Row>
        )
    }

    const columns = [
        {
            title: 'Tip',
            dataIndex: 'type',
            key: 'type',
            width: '15%'
        },
        {
            title: 'Opis',
            dataIndex: 'description',
            key: 'description',
            width: '30%'
        },
        {
            title: 'Rešen',
            key: 'status',
            dataIndex: 'status',
            width: '5%',
            render: status => (
                <>
                    {status.map(tag => {
                        let color = tag.length > 5 ? 'geekblue' : 'green';
                        if (tag === 'loser') {
                            color = 'volcano';
                        }
                        return (
                            <Tag color={color} key={tag}>
                                {tag.toUpperCase()}
                            </Tag>
                        );
                    })}
                </>
            ),
        },
        {
            title: 'Kreirano',
            dataIndex: 'created',
            key: 'created',
            width: '15%'
        },
        {
            title: 'Akcije',
            key: 'akcije',
            width: '5%',
            render: (report) => (
                <>
                    <Space size="middle">
                        <CustomTooltip title="Označi da je rešeno">
                            <Button icon={<CheckOutlined />} />
                        </CustomTooltip>
                    </Space>
                </>
            )
        },
    ];

    const report = [
        {
            key: '1',
            type: 'Heartbeat prestao',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ',
            status: ['Rešen'],
            created: '20.04.2022 14:20:33',
        },
        {
            key: '2',
            type: 'Heartbeat prestao',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ',
            status: ['Rešen'],
            created: '20.04.2022 14:20:33',
        },
        {
            key: '3',
            type: 'Heartbeat prestao',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ',
            status: ['Rešen'],
            created: '20.04.2022 14:20:33',
        },
        {
            key: '4',
            type: 'Heartbeat prestao',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ',
            status: ['Rešen'],
            created: '20.04.2022 14:20:33',
        },
    ];

    // Report table
    const reportTable = () => {
        return (
            <Table loading={isLoading} columns={columns} bordered dataSource={report} />
        )
    }

    return <>
        <CustomTopInfo>Opšti problemi</CustomTopInfo>

        <section className='card-box'>
            {pageHeader()}

            {reportFilters()}

            {reportTable()}
        </section>
    </>
}