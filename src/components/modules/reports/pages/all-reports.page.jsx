import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

//Antd
import { Table, Select, Row, Col, Button, Space, } from 'antd';

//Icons
import {
    ReloadOutlined, 
} from '@ant-design/icons'; 

//Custom components
import CustomSearchInput from '../../../UI/atoms/CustomSearchInput';
import { config } from '../../../../config/config';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';

export const AllReports = () => {


    const { Option } = Select;

    // Use states
    const [isLoading, setIsLoading] = useState(false);
    const [report, setReport] = useState([]);

    //Use effects
    useEffect(() => {
        getAllReports();
        setIsLoading(true);
    }, []);


    // Reports filters
    const reportFilters = () => {
        return (
            <Row gutter={[24, 24]} className='mb-4' justify='start'>
                <Col flex="auto" className='d-flex align-center'>
                    <Link to="/reports/general/">Opšti problemi: 5 nerešenih problema</Link>
                </Col>
                <Col span={5}><CustomSearchInput placeholder="Pretraži po nazivu uređaja" /></Col>
                <Col span={5}><CustomSearchInput placeholder="Pretraži po adresi" /></Col>
                <Col span={5}><CustomSearchInput placeholder="Pretraži po kompaniji" /></Col>
                <Col><Button icon={<ReloadOutlined />} /></Col>
            </Row>
        )
    }

    //Get all reports - AXIOS
    const getAllReports = () => {
        return (
            axios.get(config.JSON_SERVER + 'reports').then(function (response) {
                // handle success
                // console.log(response);
                setReport(response.data);
                setTimeout(() => {
                    setIsLoading(false);
                }, 500)


            })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
        )
    }


    const columns = [
        {
            title: 'uređaj',
            dataIndex: 'device',
            key: 'device',
            width: '30%'
        },
        {
            title: 'Adresa',
            dataIndex: 'address',
            key: 'address',
        },
        {
            title: 'Kompanija',
            dataIndex: 'company',
            key: 'company',
        },
        {
            title: 'Akcije',
            key: 'akcije',
            width: '15%',
            render: (report) => (
                <>
                    <Space size="middle">
                        <Link to={`/reports/${report.id}`}>{report.issue} nerešenih problema</Link>
                    </Space>
                </>
            )
        },
    ];

    // Report table
    const reportTable = () => {
        return (
            <Table rowKey={(report) => report.id} loading={isLoading} columns={columns} bordered dataSource={report} />
        )
    }

    return <>
        <CustomTopInfo>Izveštaji</CustomTopInfo>

        <section className='card-box'>
            {reportFilters()}

            {reportTable()}
        </section>
    </>
}