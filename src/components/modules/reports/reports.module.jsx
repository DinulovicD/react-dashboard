import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { AllReports } from "./pages/all-reports.page";
import { OneReport } from "./pages/one-reports.page";
import { GeneralReports } from "./pages/general-reports.page";

export const Reports = () => { 
  return (
    <Routes>  
      <Route path="/">
        <Route path="/" element={<AllReports />} />
        <Route path="/:id" element={<OneReport />} />
        <Route path="/general" element={<GeneralReports />} />
      </Route>
    </Routes>
  );
};
