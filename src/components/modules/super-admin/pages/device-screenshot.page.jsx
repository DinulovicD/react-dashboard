import React, { useEffect, useRef, useState } from 'react';

//Antd
import { Row, Col, Button, PageHeader } from 'antd';

//Icons
import {
  BellOutlined,
  CheckOutlined
} from '@ant-design/icons';
import { BiScreenshot } from 'react-icons/bi';

//Custom components
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';

export const DeviceScreenshot = () => {


  const pageHeader = () => {
    return (
      <>
        <PageHeader
          className="pl-0 pr-0"
          ghost={false}
          onBack={() => window.history.back()}
          title="Nazad"
          extra={[
            <Button
              key="1"
              type="primary"
            >
              Aktiviraj notifikacije
              <BellOutlined />
            </Button>,
            <Button
              key="2"
              type="primary"
            >
              Pošalji zahtev za screenshot
              <BiScreenshot />
            </Button>,
          ]}
        ></PageHeader>
      </>
    );
  };


  return <>
    <CustomTopInfo>Sandra phone - screenshots</CustomTopInfo>

    <section className='card-box'>
      {pageHeader()}

    </section>
  </>
}
