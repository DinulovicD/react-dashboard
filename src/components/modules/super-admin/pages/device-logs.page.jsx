import React, { useState } from 'react';
import { Link } from 'react-router-dom';

//Ant 
import { Table, Select, Row, Col, Input, Button, DatePicker, Space, PageHeader } from 'antd';

//Icons
import {
  ReloadOutlined,
  LinkOutlined,
  DownloadOutlined
} from '@ant-design/icons';

// Custom components
import CustomSelect from '../../../UI/atoms/CustomSelect';
import CustomTooltip from '../../../UI/atoms/CustomTooltip';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';


export const DeviceLogs = () => {
 
  const { Option } = Select;
  const { Search } = Input;
  const { RangePicker } = DatePicker;

  //Use states
  const [filters, setFilters] = useState({});

  const columns = [
    {
      title: 'Akcija',
      dataIndex: 'action',
      key: 'action',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: 'Kreirano',
      dataIndex: 'created',
      key: 'created',
    },
    {
      title: 'Izmenjeno',
      dataIndex: 'modified',
      key: 'modified',
    },
    {
      title: 'Akcije',
      key: 'akcije',
      width: '5%',
      render: (user) => (
        <>
          <Space size="middle">
            <CustomTooltip title="Vidi detalje">
              <Link to="/devices/super-admin/log/1"><Button className="mr-2 d-flex align-center justify-center" icon={<LinkOutlined />} /></Link>
            </CustomTooltip>
          </Space>
        </>
      )
    },
  ];

  const data = [
    {
      key: '1',
      action: 'test company fake',
      status: 'Uspešno',
      created: '04.03.2022 14:40:37',
      modified: '04.03.2022 14:40:37',
    },

    {
      key: '2',
      action: 'test company fake',
      status: 'Uspešno',
      created: '04.03.2022 14:40:37',
      modified: '04.03.2022 14:40:37',
    },

    {
      key: '3',
      action: 'test company fake',
      status: 'Uspešno',
      created: '04.03.2022 14:40:37',
      modified: '04.03.2022 14:40:37',
    },

    {
      key: '4',
      action: 'test company fake',
      status: 'Uspešno',
      created: '04.03.2022 14:40:37',
      modified: '04.03.2022 14:40:37',
    },

    {
      key: '5',
      action: 'test company fake',
      status: 'Uspešno',
      created: '04.03.2022 14:40:37',
      modified: '04.03.2022 14:40:37',
    },

    {
      key: '6',
      action: 'test company fake',
      status: 'Uspešno',
      created: '04.03.2022 14:40:37',
      modified: '04.03.2022 14:40:37',
    },


  ];

  const resetFilters = () => {
    setFilters({});
  }



  // Device filters
  const deviceFilters = () => {
    return (
      <Row gutter={[24, 24]} className='mb-4' justify='end'>
        <Col flex="auto">
          <CustomSelect
            style={{ width: '100%' }}
            showSearch
            placeholder="Akcija"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            } >
            <Option value="Jack">Broj prikaza</Option>
            <Option value="Dzon">Vreme prikaza</Option>
          </CustomSelect>
        </Col>
        <Col flex="auto">
          <CustomSelect
            style={{ width: '100%' }}
            showSearch
            placeholder="Status"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            } >
            <Option value="Jack">Rastuće</Option>
            <Option value="Dzon">Opadajuće</Option>
          </CustomSelect>
        </Col>
        <Col flex="auto"><RangePicker style={{ width: '100%' }} /></Col>
        <Col><Button icon={<ReloadOutlined />} onClick={() => resetFilters()} /></Col>
      </Row>
    )
  }

  // Device table
  const deviceTable = () => {
    return (
      <Table columns={columns} bordered dataSource={data} />
    )
  }


  const pageHeader = () => {
    return (
      <>
        <PageHeader
          className="pl-0 pr-0"
          ghost={false}
          onBack={() => window.history.back()}
          title="Nazad"
          extra={[
              <Button
              key="1" 
              htmlType="submit"
              type="primary"
            >
              Skini logove sa servera
              <DownloadOutlined />
            </Button> ,
          ]}
        ></PageHeader>
      </>
    );
  };

  return <>

<CustomTopInfo>Sandra phone - Logovi sa servera</CustomTopInfo>


    <section className='card-box'>
      {pageHeader()}

      {deviceFilters()}

      {deviceTable()}
    </section>
  </>;
}
