import React, { useState } from 'react';

//Ant 
import { Table, Select, Row, Col, Input, Button, DatePicker, Space, PageHeader } from 'antd';

// Custom components
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';


export const OneDeviceLog = () => {

    const { Option } = Select;
    const { Search } = Input;
    const { RangePicker } = DatePicker;

    //Use states
    const [filters, setFilters] = useState({});

    const columns = [
        {
            title: 'Redni broj',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Naziv (sa prefiksom)',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Hash',
            dataIndex: 'hash',
            key: 'hash',
        },
    ];

    const data = [
        {
            key: '1',
            id: '1',
            name: 'SA_nike.jpg',
            hash: '0a711040b39c488d357ecec54eac31b1',
        },

        {
            key: '2',
            id: '2',
            name: 'SA_nike.jpg',
            hash: '0a711040b39c488d357ecec54eac31b1',
        },

        {
            key: '3',
            id: '3',
            name: 'SA_nike.jpg',
            hash: '0a711040b39c488d357ecec54eac31b1',
        },

        {
            key: '4',
            id: '4',
            name: 'SA_nike.jpg',
            hash: '0a711040b39c488d357ecec54eac31b1',
        },

        {
            key: '5',
            id: '5',
            name: 'SA_nike.jpg',
            hash: '0a711040b39c488d357ecec54eac31b1',
        },

        {
            key: '6',
            id: '6',
            name: 'SA_nike.jpg',
            hash: '0a711040b39c488d357ecec54eac31b1',
        },


    ];

    const resetFilters = () => {
        setFilters({});
    }

    // Device table
    const deviceTable = () => {
        return <>
            <h3 className="card-title">Playlista</h3>
            <Table columns={columns} pagination={false} bordered dataSource={data} />
        </>
    }

    const databaseJson = `{
        "playlist_data": {
            "media": [
                {
                    "id": 118,
                    "url": "https:\/\/oneviewtest.itcentar.rs\/storage\/media\/superadmin\/A1lqcMpCxxabUWMhTBYG7dO1fwkYUl.jpg",
                    "type": 1,
                    "duration": 10,
                    "name": "SA_nike.jpg",
                    "md5": "0a711040b39c488d357ecec54eac31b1",
                    "user_id": null,
                    "company_id": null
                },
                {
                    "id": 26,
                    "url": "https:\/\/oneviewtest.itcentar.rs\/storage\/media\/superadmin\/lOSxHvE5gSVDxa3niA2juBnTzeaBsC.jpg",
                    "type": 1,
                    "duration": 10,
                    "name": "SA_Slika.jpg",
                    "md5": "fee27049fb9dff20baafec1b4028e6cc",
                    "user_id": null,
                    "company_id": null
                },
                {
                    "id": 27,
                    "url": "https:\/\/oneviewtest.itcentar.rs\/storage\/media\/superadmin\/nX6S1y8CR8ZjQ6nWAxsvFRnHTecrbc.jpg",
                    "type": 1,
                    "duration": 10,
                    "name": "SA_logo.jpg.jpg",
                    "md5": "efaf35bbb436cf2727d19db9daa46c90",
                    "user_id": null,
                    "company_id": null
                },
                {
                    "id": 440,
                    "url": "https:\/\/oneviewtest.itcentar.rs\/storage\/media\/superadmin\/2yvqMxQcXXWzHML0ORQqOdlLaiFJI6.jpg",
                    "type": 1,
                    "duration": 10,
                    "name": "SA_lon3 master.jpg",
                    "md5": "c609d3e31787b6f3073acc28e51582fc",
                    "user_id": 69,
                    "company_id": null
                },
                {
                    "id": 439,
                    "url": "https:\/\/oneviewtest.itcentar.rs\/storage\/media\/superadmin\/jeib81hcq2yhCGOy38B5YCvlJGQxXn.jpg",
                    "type": 1,
                    "duration": 10,
                    "name": "SA_lon1 master.jpg",
                    "md5": "bde67ebec85b3fe55efc7ccd53076775",
                    "user_id": 69,
                    "company_id": null
                }
            ]
        }
    }`

    const dataFromDatabase = () => {
        return <>
            <h3 className="card-title mt-5">Podaci u bazi</h3>
            <div className="database-json">
                <pre>
                    <code>{databaseJson}</code>
                </pre>
            </div>
        </>
    }


    const pageHeader = () => {
        return (
            <>
                <PageHeader
                    className="pl-0 pr-0"
                    ghost={false}
                    onBack={() => window.history.back()}
                    title="Nazad"
                ></PageHeader>
            </>
        );
    };

    return <>

        <CustomTopInfo>Sandra phone - log 1</CustomTopInfo>


        <section className='card-box'>
            {pageHeader()}

            {deviceTable()}

            {dataFromDatabase()}
        </section>
    </>;
}