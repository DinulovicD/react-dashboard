import React, { useState } from 'react';
import { Link } from 'react-router-dom';

//Ant 
import { Table, Select, Row, Col, Input, Button, Space } from 'antd';

//Icons
import {
    ReloadOutlined,
    DeleteOutlined,
    EditOutlined,
    CloudServerOutlined,
    FileDoneOutlined,
    QuestionCircleOutlined
} from '@ant-design/icons';
import { AiOutlinePlus } from "react-icons/ai";
import { BiScreenshot } from 'react-icons/bi';
import { MdOutlineAdminPanelSettings } from "react-icons/md";

// Custom components
import CustomSelect from '../../../UI/atoms/CustomSelect';
import CustomSearchInput from '../../../UI/atoms/CustomSearchInput';
import CustomTooltip from '../../../UI/atoms/CustomTooltip';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';

export const OneSuperAdmin = () => {



    const { Option } = Select;
    const { Search } = Input;

    //Use states
    const [filters, setFilters] = useState({});

    const columns = [
        {
            title: 'Naziv',
            dataIndex: 'naziv',
            key: 'naziv',
        },
        {
            title: 'Broj prikaza',
            dataIndex: 'views',
            key: 'views',
        },
        {
            title: 'Vreme prikaza',
            dataIndex: 'date_views',
            key: 'date_views',
        },
    ];

    const data = [
        {
            key: '1',
            naziv: 'test company fake',
            views: '129',
            date_views: '00:21:30',
        },

        {
            key: '2',
            naziv: 'test company fake',
            views: '129',
            date_views: '00:21:30',
        },

        {
            key: '3',
            naziv: 'test company fake',
            views: '129',
            date_views: '00:21:30',
        },

        {
            key: '4',
            naziv: 'test company fake',
            views: '129',
            date_views: '00:21:30',
        },

        {
            key: '5',
            naziv: 'test company fake',
            views: '129',
            date_views: '00:21:30',
        },

        {
            key: '6',
            naziv: 'test company fake',
            views: '129',
            date_views: '00:21:30',
        },


    ];

    const resetFilters = () => {
        setFilters({});
    }



    // Device filters
    const deviceFilters = () => {
        return (
            <Row gutter={[24, 24]} className='mb-4' justify='end'>
                <Col flex="auto" className='d-flex'>
                    <CustomTooltip title="Screenshotovi"><Link to="/devices/super-admin/screenshots"><Button className="mr-3 d-flex" icon={<BiScreenshot />} /></Link></CustomTooltip>
                    <CustomTooltip title="Logovi sa servera"><Link to="/devices/super-admin/logs"><Button className="mr-3" icon={<CloudServerOutlined />} /></Link></CustomTooltip>
                    <CustomTooltip title="Skini logove sa uređaja"><Button className="mr-3" icon={<FileDoneOutlined />} /></CustomTooltip>
                    <CustomTooltip title="Obriši media statistiku"><Button className="mr-3" icon={<DeleteOutlined />} /></CustomTooltip>
                </Col>
                <Col span={5}><CustomSearchInput placeholder="Pretraži po nazivu" /></Col>
                <Col span={5}>
                    <CustomSelect
                        style={{ width: '100%' }}
                        showSearch
                        placeholder="Poredjaj po"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                        filterSort={(optionA, optionB) =>
                            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                        } >
                        <Option value="Jack">Broj prikaza</Option>
                        <Option value="Dzon">Vreme prikaza</Option>
                    </CustomSelect>
                </Col>
                <Col span={4}>
                    <CustomSelect
                        style={{ width: '100%' }}
                        showSearch
                        placeholder="Rastuće"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                        filterSort={(optionA, optionB) =>
                            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                        } >
                        <Option value="Jack">Rastuće</Option>
                        <Option value="Dzon">Opadajuće</Option>
                    </CustomSelect>
                </Col>
                <Col><Button icon={<ReloadOutlined />} onClick={() => resetFilters()} /></Col>
            </Row>
        )
    }

    // Device table
    const deviceTable = () => {
        return (
            <Table columns={columns} bordered dataSource={data} />
        )
    }


    return <>

        <CustomTopInfo>Sandra phone - super admin panel</CustomTopInfo>


        <section className='card-box'>

            {deviceFilters()}

            {deviceTable()}
        </section>
    </>;
}