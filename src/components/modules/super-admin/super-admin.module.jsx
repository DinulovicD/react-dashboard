import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { OneSuperAdmin } from "./pages/one-super-admin.page";
import { DeviceScreenshot } from "./pages/device-screenshot.page";
import { DeviceLogs } from "./pages/device-logs.page";
import { OneDeviceLog } from "./pages/one-device-log.page";

export const SuperAdmin = () => { 
  return (
    <Routes>  
      <Route path="/">
        <Route path="/:id" element={<OneSuperAdmin />} />
        <Route path="/screenshots" element={<DeviceScreenshot />} />
        <Route path="/logs" element={<DeviceLogs />} />
        <Route path="/log/:id" element={<OneDeviceLog />} /> 
      </Route>
    </Routes>
  );
};
