import React, { useState, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";

//Ant
import { Col, message, notification, Row } from 'antd';

//Formik
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';

//Custom components
import CustomInput from '../../../UI/atoms/CustomInput.jsx';
import CustomButton from '../../../UI/atoms/CustomButton.jsx';

import { loginRequest } from "../../../../services/auth.service";
import { UserContext } from "../../../../contexts/user.context";

//Icons
import {
    LockOutlined,
    CheckCircleOutlined,
    EyeOutlined,
    EyeInvisibleOutlined
} from '@ant-design/icons';

export const Forgot = () => {

    const loginValidationSchema = Yup.object().shape({
        email: Yup.string().email('Email nije ispravna!').required('Unesite email adresu'),
    });

    const navigate = useNavigate();
    const [submitLoading, setSubmitLoading] = useState(false);
    const { login } = useContext(UserContext);


    const [passwordType, setPasswordType] = useState("password");
    const [passwordInput, setPasswordInput] = useState("");
    const handlePasswordChange = (evnt) => {
        setPasswordInput(evnt.target.value);
    }
    const togglePassword = () => {
        if (passwordType === "password") {
            setPasswordType("text")
            return;
        }
        setPasswordType("password")
    }


    return <>
        <Formik
            validationSchema={loginValidationSchema}
            initialValues={{
                email: '', 
            }}
            onSubmit={(values, actions) => {
                console.log('SUBMIT', values);
                setSubmitLoading(true);
                loginRequest(values)
                    .then((res) => {
                        console.log('res', res);
                        login(res.data);
                        navigate("/", {
                            replace: true,
                        });

                        notification.open({
                            message: 'Uspešno ste se ulogovali!',
                            icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
                        });
                    })
                    .catch((err) => {
                        console.log(err);
                        const messages = err?.messages;

                        if (messages) {
                            messages.forEach((msg) => {
                                message.error({
                                    content: msg,
                                    duration: 3,
                                });
                            });
                        } else {
                            message.error({
                                content: "something_went_wrong",
                                duration: 3,
                            });
                        }
                    })
                    .finally(() => {
                        setSubmitLoading(false);
                    });

            }}
        >
            {(props) => (
                <Form>
                    <Row gutter={[24, 24]}>
                        <Col span={24}>
                            <h2>Resetovanje lozinke</h2>
                            <h3>Prijavite se da nastavite do OneView.</h3>
                        </Col>

                        <Col span={24}>
                            <Field name="email" type="email" label="Email" component={CustomInput} />
                            {props.errors.email && props.touched.email ? (
                                <div className="error-text">{props.errors.email}</div>
                            ) : null}
                        </Col>

                        <Col span={24}>
                            <CustomButton type="primary" htmlType="submit" className="d-flex align-center w-100 justify-center">Reset</CustomButton>
                        </Col>

                        <Col span={24}>
                            <p className="d-flex justify-center">Remember It ? <Link to="../login" className='forgot-password ml-1'>Login</Link></p>
                        </Col>
                    </Row>
                </Form>
            )}
        </Formik>
    </>
}; 
