import React, { useContext } from "react";

import { Outlet } from "react-router-dom";


//Slider
import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/react-splide/css';

//Images
import LogoImage from './../../../assets/images/logo-dark.svg';
import CustomImage from '../../UI/atoms/CustomImage.jsx';
import { ImQuotesLeft } from "react-icons/im";
 
export const AuthOutlet = ({ children }) => {
 
  return <>
    <section className='login'>
      <div className="login-testimonials">
        <h2><ImQuotesLeft />1k+ Zadovoljni klijenti</h2>
        <Splide
          options={{
            arrows: false,
            type: 'loop',
            autoplay: true,
          }}
          aria-label="My Favorite Images">

          <SplideSlide>
            <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos dolore, quasi provident repellendus quis tenetur enim, consequatur magni atque obcaecati ipsa eum fugiat ratione. Et inventore facere vero dicta ea?"</p>
            <p className="company-name">Irish pub</p>
            <p className="user-name">- Petar Petrovic</p>
          </SplideSlide>
          <SplideSlide>
            <p>"Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eaque veritatis voluptatum aspernatur, esse cumque labore possimus provident doloremque quidem molestiae sapiente, accusamus quisquam, necessitatibus accusantium sit quis nihil dolore. At."</p>
            <p className="company-name">Apoteka Benu</p>
            <p className="user-name">- Milan Stankovic</p>
          </SplideSlide>
        </Splide>
      </div>
      <div className="login-form">
        <div className="logo">
          <CustomImage src={LogoImage}></CustomImage>
        </div>
        <div className="form">
          <Outlet />
          {children}
        </div>
        <div className="copyright"><p>2022 © OneView. Design & Develop by <a href="https://itcentar.rs" target="_blank">IT Centar</a></p></div>
      </div>
    </section>

  </>
};
