import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { Login } from "./pages/login.page"; 
import { Forgot } from "./pages/forgot.page";
import { Reset } from "./pages/reset.page";
import { AuthOutlet } from "./auth.outlet";

export const Auth = () => {
    return (
        <Routes> 
            <Route path="/" element={<AuthOutlet />}>
                <Route path="/" element={<Navigate to={"login"} />} />
                <Route path="login" element={<Login />} /> 
                <Route path="forgot" element={<Forgot />} />
                <Route path="reset" element={<Reset />} />
                <Route path="*" element={<Navigate to={"login"} />} />
            </Route>
        </Routes>
    );
};
