import React, { useState } from 'react';
import { Link } from 'react-router-dom';

//Ant
import { Table, Select, Row, Col, Input, Button, Space, Modal } from 'antd';

//Icons
import {
  ReloadOutlined,
  DeleteOutlined,
  EditOutlined,
  QuestionCircleOutlined
} from '@ant-design/icons';
import { AiOutlinePlus } from "react-icons/ai";
import { FiMonitor } from 'react-icons/fi';

// Custom components
import CustomSelect from '../../../UI/atoms/CustomSelect';
import CustomSearchInput from '../../../UI/atoms/CustomSearchInput';
import CustomTooltip from '../../../UI/atoms/CustomTooltip';
import CustomPopConfirm from '../../../UI/atoms/CustomPopConfirm';
import CustomInput from '../../../UI/atoms/CustomInput';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';

export const AllGroupDevices = () => {

  //Use states
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [filters, setFilters] = useState({});

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };


  const { Option } = Select;
  const { Search } = Input;

  const columns = [
    {
      title: 'Naziv',
      dataIndex: 'naziv',
      key: 'naziv',
    },
    {
      title: 'uređaji',
      dataIndex: 'uređaji',
      key: 'uređaji',
    },
    {
      title: 'Plejlista',
      dataIndex: 'plejlista',
      key: 'plejlista',
    },
    {
      title: 'Akcije',
      key: 'akcije',
      width: '10%',
      render: (user) => (
        <>
          <Space size="middle">
            <CustomTooltip title="Izmeni"> <Button className="mr-2 d-flex align-center justify-center" onClick={showModal} icon={<EditOutlined />} /></CustomTooltip>

            <CustomTooltip title="Obriši">
              <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                <Button className="mr-2 d-flex align-center justify-center" icon={<DeleteOutlined />} />
              </CustomPopConfirm>
            </CustomTooltip>
          </Space>
        </>
      )
    },
  ];

  const data = [
    {
      key: '1',
      naziv: 'test company fake',
      uređaji: 'fake test android box 1',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },

    {
      key: '2',
      naziv: 'test company fake',
      uređaji: 'fake test android box 1',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },

    {
      key: '3',
      naziv: 'test company fake',
      uređaji: 'fake test android box 1',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },

    {
      key: '4',
      naziv: 'test company fake',
      uređaji: 'fake test android box 1',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },

    {
      key: '5',
      naziv: 'test company fake',
      uređaji: 'fake test android box 1',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },

    {
      key: '6',
      naziv: 'test company fake',
      uređaji: 'fake test android box 1',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },

    {
      key: '7',
      naziv: 'test company fake',
      uređaji: 'fake test android box 1',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },


  ];

  const onSearch = value => console.log(value);

  const resetFilters = () => {
    setFilters({});
  }

  const groupDeviceFilters = () => {
    return (
      <Row gutter={[24, 24]} className='mb-4' justify='end'>
        <Col flex="auto" className='d-flex'>
          <Button type="primary" onClick={showModal} className='d-flex align-center mr-3'> Dodaj grupu uređaja <AiOutlinePlus /></Button>
          <Link to="/group-devices/edit"><Button type="primary" className='d-flex align-center'> Izmeni više grupa <EditOutlined className='ml-0' /></Button></Link>
        </Col>
        <Col span={5}><CustomSearchInput placeholder="Pretraži email adrese" onSearch={onSearch} /></Col>
        <Col span={5}>
          <CustomSelect
            style={{ width: '100%' }}
            showSearch
            placeholder="Izaberite plejlistu"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            } >
            <Option value="Jack">Jack</Option>
            <Option value="Dzon">Dzon</Option>
          </CustomSelect>
        </Col>
        <Col><Button icon={<ReloadOutlined />} onClick={() => resetFilters()} /></Col>
      </Row>
    )
  }

  const groupDeviceTable = () => {
    return (
      <Table columns={columns} bordered dataSource={data} />
    )
  }

  const groupDeviceModal = () => {
    return (
      <Modal centered title="Izmeni naziv grupe" visible={isModalVisible} okText="Sačuvaj"
      cancelText="Odustani" onOk={handleOk} onCancel={handleCancel}>
        <CustomInput label="Naziv grupe"></CustomInput>
      </Modal>
    )
  }


  return <>
        <CustomTopInfo>Grupe uređaja</CustomTopInfo>

    <section className='card-box'>
      {groupDeviceFilters()}

      {groupDeviceTable()}

      {groupDeviceModal()}
    </section>

  </>;

};
