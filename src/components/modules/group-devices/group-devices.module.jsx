import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { AllGroupDevices } from "./pages/all-group-devices.page";
import { EditManyGroupDevices } from "./pages/edit-many-group-devices.page"; 

export const GroupDevices = () => { 
  return (
    <Routes> 
      <Route path="/">
        <Route path="/" element={<AllGroupDevices />} />
        <Route path="/edit" element={<EditManyGroupDevices />} /> 
      </Route>
    </Routes>
  );
};
