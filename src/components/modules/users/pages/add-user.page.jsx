import React, { useState, useEffect } from 'react';

//Antd
import { Button, Col, notification, PageHeader, Row, Select, Skeleton } from 'antd';

//Icons 
import {
    CheckCircleOutlined,
} from '@ant-design/icons';
import { BsSave } from "react-icons/bs";
import { AiOutlineInfoCircle } from "react-icons/ai";

//Formik
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';


//Users service
import { createUserRequest } from "../../../../services/users.service";

//Custom components
import CustomInput from '../../../UI/atoms/CustomInput';
import axios from 'axios';
import { config } from '../../../../config/config';
import CustomSelect from '../../../UI/atoms/CustomSelect';
import CustomCheckbox from '../../../UI/atoms/CustomCheckbox';
import CustomTooltip from '../../../UI/atoms/CustomTooltip';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';



export const AddUser = () => {

    const userValidationSchema = Yup.object().shape({
        name: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Obavezno polje!'),
        email: Yup.string().email('Email nije ispravan!').required('Obavezno polje!'),
    });


    const { Option } = Select;

    //Use state
    const [isLoading, setIsLoading] = useState(false);
    const [isRole, setIsRole] = useState(null);
    const [formInitValues, setFormInitValue] = useState({
        name: '',
        email: '',
        phone: '',
        role: '1',
        password: '',
    });

    //Header page
    const header = () => {
        return (
            <>
                <PageHeader
                    className="pl-0 pr-0"
                    ghost={false}
                    onBack={() => window.history.back()}
                    title="Nazad"
                    extra={[
                        <Button
                            key="1"
                            htmlType="submit"
                            type="primary"
                        >
                            Sačuvaj
                            <BsSave />
                        </Button>,
                    ]}
                ></PageHeader>
            </>
        );
    };


    const handleRole = () => {
        switch (isRole) {
            case "admin":
            case "korisnik":
            case "urednik_kajrona":
                return <Col span={8}>
                    <label htmlFor="company">Kompanije</label>
                    <CustomSelect
                        style={{ width: '100%' }}
                        showSearch
                        id="company"
                        defaultValue="0"
                        optionFilterProp="children">
                        <Option value="0">IT Centar</Option>
                        <Option value="1">Itcentar</Option>
                        <Option value="2">Itcentar</Option>
                        <Option value="3">Itcentar</Option>
                    </CustomSelect>
                </Col>;
            case "oglasivac":
                return <>
                    <Col span={24}>
                        <h2 className='mb-0 card-title mt-3'>uređaji</h2>
                    </Col>

                    <Col span={6}>
                        <div className="edit-many-devices-list">
                            <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
                            <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" icon={<AiOutlineInfoCircle />} /></CustomTooltip>
                        </div>
                    </Col>

                    <Col span={6}>
                        <div className="edit-many-devices-list">
                            <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
                            <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" icon={<AiOutlineInfoCircle />} /></CustomTooltip>
                        </div>
                    </Col>

                    <Col span={6}>
                        <div className="edit-many-devices-list">
                            <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
                            <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" icon={<AiOutlineInfoCircle />} /></CustomTooltip>
                        </div>
                    </Col>

                    <Col span={6}>
                        <div className="edit-many-devices-list">
                            <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
                            <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" icon={<AiOutlineInfoCircle />} /></CustomTooltip>
                        </div>
                    </Col>
                </>
            default:
                return null;
        }
    }

    //Add user form
    const addUserForm = () => {
        return (
            <Formik
                initialValues={formInitValues}
                enableReinitialize={true}
                validationSchema={userValidationSchema}
                onSubmit={(values, actions) => {

                    //Axios send data
                    createUserRequest(values).then(function (response) {
                        // handle success
                        console.log(response);

                        notification.open({
                            message: 'Dodavanje korisnika',
                            description:
                                'Uspešno ste dodali korisnika.',
                            icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
                        });

                        setTimeout(() => {
                            setIsLoading(false); 
                        }, 500)

                    })
                        .catch(function (error) {
                            // handle error
                            console.log(error.errors);
                            // let errors;
                            // error.forEach(function(obj){ console.log(obj.id); });

                            notification.open({
                                message: 'Dodavanje korisnika',
                                // description: errors,
                                icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
                            });
                        })
                        .then(function () {
                            // always executed
                        })

                }}
            >
                {(props) => (
                    <Form>
                        {header()}
                        <Row gutter={[24, 12]}>
                            <Col span={8}>
                                <Field name="name" type="text" label="Ime i prezime" component={CustomInput} />
                                {props.errors.name && props.touched.name ? (
                                    <div className="error-text">{props.errors.name}</div>
                                ) : null}
                            </Col>

                            <Col span={8}>
                                <Field name="email" type="email" label="Email" component={CustomInput} />
                                {props.errors.email && props.touched.email ? <div className="error-text">{props.errors.email}</div> : null}
                            </Col>

                            <Col span={8}>
                                <Field name="phone" type="tel" label="Telefon" component={CustomInput} />
                            </Col>

                            <Col span={8}>
                                <label htmlFor="role">Uloga</label>
                                <CustomSelect
                                    style={{ width: '100%' }}
                                    showSearch
                                    id="role"
                                    name="role"
                                    defaultValue="1"
                                    onChange={(event) => {
                                        // console.log(event);
                                        setIsRole(event);
                                    }}
                                    optionFilterProp="children">
                                    <Option value="1">Super admin</Option>
                                    <Option value="admin">Admin</Option>
                                    <Option value="korisnik">Korisnik</Option>
                                    <Option value="urednik_kajrona">Urednik Kajrona</Option>
                                    <Option value="oglasivac">Oglašivač</Option>
                                </CustomSelect>
                            </Col>

                            <Col span={8}>
                                <Field name="password" type="password" label="Lozinka" component={CustomInput} />
                            </Col>

                            <Col span={8}>
                                <Field name="password_repeat" type="password" label="Ponovite lozinku" component={CustomInput} />
                            </Col>

                            {handleRole()}
                        </Row>

                    </Form>
                )}
            </Formik>
        );
    }


    return <>

        <Skeleton active loading={isLoading} paragraph={{ rows: 6 }}>

            <CustomTopInfo>Dodaj korisnika</CustomTopInfo>

            <section className='card-box'>
                {addUserForm()}
            </section>
            
        </Skeleton>

    </>;
};
