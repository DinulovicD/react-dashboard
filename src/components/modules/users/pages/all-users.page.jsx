import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

//Antd
import { Table, Select, Row, Col, Input, Button, Space, notification, Spin } from 'antd';

//Icons
import {
  ReloadOutlined,
  DeleteOutlined,
  CheckCircleOutlined,
  EditOutlined,
  QuestionCircleOutlined
} from '@ant-design/icons';
import { AiOutlinePlus } from "react-icons/ai";

//Users service
import { getUsersRequest, deleteUserRequest } from "../../../../services/users.service";

//Custom components
import CustomSelect from '../../../UI/atoms/CustomSelect';
import CustomSearchInput from '../../../UI/atoms/CustomSearchInput';
import CustomTooltip from '../../../UI/atoms/CustomTooltip';
import CustomPopConfirm from '../../../UI/atoms/CustomPopConfirm';
import CustomButton from '../../../UI/atoms/CustomButton';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';



export const AllUsers = () => {


  const { Option } = Select;
  const { Search } = Input;

  //Use states
  const [data, setData] = useState([]);
  const [filters, setFilters] = useState({});
  const [options, setOptions] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [state, setState] = useState({
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
  });

  //Use effects
  useEffect(() => {

    //Users
    getAllUsers();
    setIsLoading(true);

  }, []);

  //Get all users - AXIOS
  const getAllUsers = () => {
    return (
      getUsersRequest().then(function (response) {
        // handle success 
        console.log(response.data);
        setData(response.data.data);
        setTimeout(() => {
          setIsLoading(false);
        }, 500)


      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }

  // Delete user by id - AXIOS
  const deleteUser = (id) => {
    return (
      deleteUserRequest(id).then(function (response) {
        // handle success
        console.log(response);
        getAllUsers();

        notification.open({
          message: 'Brisanje korisnika',
          description:
            'Uspešno ste obrisali korisnika.',
          icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
        });


      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }


  //Confirmation button for delete user
  const deleteUserOk = (id) => {
    setTimeout(() => {
      <Spin />
      deleteUser(id);
    }, 1000)
  }


  //Search email address
  const onSearchEmailAddress = (value) => {
    setFilters({
      ...filters,
      email_like: value
    })
  }

  //On change company filter
  const onChangeRoleFilter = (value) => {

    setFilters({
      ...filters,
      role_like: value
    });

  }

  const resetFilters = () => {
    setFilters({});
  }

  /*
  * Main content
  */

  const columns = [
    {
      title: 'Ime',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Telefon',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'Uloga',
      dataIndex: 'role',
      key: 'role',
    },
    {
      title: 'Kompanija',
      dataIndex: 'company',
      key: 'company',
    },
    {
      title: 'Akcije',
      key: 'akcije',
      width: '10%',
      render: (data) => (
        <>
          <Space size="middle">
            <CustomTooltip title="Izmeni"><Link to={`/users/edit/${data.id}`}><Button className="mr-2 d-flex align-center justify-center" icon={<EditOutlined />} /></Link></CustomTooltip>

            <CustomTooltip title="Obriši">
              <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" onConfirm={() => deleteUserOk(data.id)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                <Button className="mr-2 d-flex align-center justify-center" icon={<DeleteOutlined />} />
              </CustomPopConfirm>
            </CustomTooltip>
          </Space>
        </>
      )
    },
  ];

  //User filters
  const userFilters = () => {
    return (
      <Row gutter={[24, 24]} className='mb-4' justify='end'>
        <Col flex="auto">
          <Link to="/users/create"><Button type="primary" className='d-flex align-center'> Dodaj novog korisnika <AiOutlinePlus /></Button></Link>
        </Col>
        <Col span={5}>
          <CustomSelect
            style={{ width: '100%' }}
            showSearch
            placeholder="Izaberite kompaniju"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            } >
            <Option value="Jack">Jack</Option>
            <Option value="Dzon">Dzon</Option>
          </CustomSelect>
        </Col>
        <Col span={5}>
          <CustomSelect
            style={{ width: '100%' }}
            showSearch
            placeholder="Izaberite ulogu"
            optionFilterProp="children"
            options={options}
            onChange={onChangeRoleFilter} >
          </CustomSelect>
        </Col>
        <Col span={5}><CustomSearchInput placeholder="Pretraži email adrese" onSearch={onSearchEmailAddress} /></Col>
        <Col><CustomTooltip title="Obriši izabrane stavke"><Button icon={<DeleteOutlined />} onClick={start} disabled={!hasSelected} loading={loading} /></CustomTooltip></Col>
        <Col><CustomTooltip title="Resetuj filtere"><Button icon={<ReloadOutlined />} onClick={() => resetFilters()} /></CustomTooltip></Col>
      </Row>
    )
  }

  const start = () => {
    setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      setState({
        selectedRowKeys: [],
        loading: false,
      });
    }, 1000);
  };

  const onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setState({ selectedRowKeys });
  };

  //User table
  const { loading, selectedRowKeys } = state;
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;
  const userTable = () => {
    return <>

   
      <span className='mb-4'>{hasSelected ? `Izabrane su ${selectedRowKeys.length} stavke` : ''}</span>
    
      <Table rowSelection={rowSelection} rowKey={(data) => data.id} loading={isLoading} columns={columns} bordered dataSource={data} />
    </>
  }


  return <>
    <CustomTopInfo>Korisnici</CustomTopInfo>

    <section className='card-box'>

      {userFilters()}

      {userTable()}
    </section>
  </>;
};

