import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom'; 

//Antd
import { Button, Select, Col, notification, PageHeader, Row, Skeleton } from 'antd';

//Formik
import { Formik, Form, Field } from 'formik';

//Icons
import {
  CheckCircleOutlined,
} from '@ant-design/icons';
import { BsSave } from "react-icons/bs";
import { AiOutlineInfoCircle } from "react-icons/ai";

//User service
import { updateUserRequest } from "../../../../services/users.service";

//Custom components
import CustomInput from '../../../UI/atoms/CustomInput'; 
import CustomSelect from '../../../UI/atoms/CustomSelect';
import CustomCheckbox from '../../../UI/atoms/CustomCheckbox';
import CustomTooltip from '../../../UI/atoms/CustomTooltip';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';




export const EditUser = () => {

  const { id: userId } = useParams();
  const { Option } = Select;

  //Use states
  const [userData, setUserData] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [isRole, setIsRole] = useState(null);
  const [formInitValues, setFormInitValue] = useState({
    name: '',
    email: '',
    phone: '',
    role: '',
    company: '',
  });


  //Use effects
  useEffect(() => {
    getOne(userId);

    setIsLoading(true);

    setFormInitValue({
      name: userData.name,
      email: userData.email,
      phone: userData.phone,
      role: userData.role,
      company: userData.company,
    })

  }, []);

  const handleRole = () => {
    switch (isRole) {
      case "admin":
      case "korisnik":
      case "urednik_kajrona":
        return <Col span={8}>
          <label htmlFor="company">Kompanije</label>
          <CustomSelect
            style={{ width: '100%' }}
            showSearch
            id="company"
            defaultValue="0"
            optionFilterProp="children">
            <Option value="0">IT Centar</Option>
            <Option value="1">Itcentar</Option>
            <Option value="2">Itcentar</Option>
            <Option value="3">Itcentar</Option>
          </CustomSelect>
        </Col>;
      case "oglasivac":
        return <>
          <Col span={24}>
            <h2 className='mb-0 card-title mt-3'>uređaji</h2>
          </Col>

          <Col span={6}>
            <div className="edit-many-devices-list">
              <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
              <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" icon={<AiOutlineInfoCircle />} /></CustomTooltip>
            </div>
          </Col>

          <Col span={6}>
            <div className="edit-many-devices-list">
              <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
              <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" icon={<AiOutlineInfoCircle />} /></CustomTooltip>
            </div>
          </Col>

          <Col span={6}>
            <div className="edit-many-devices-list">
              <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
              <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" icon={<AiOutlineInfoCircle />} /></CustomTooltip>
            </div>
          </Col>

          <Col span={6}>
            <div className="edit-many-devices-list">
              <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
              <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" icon={<AiOutlineInfoCircle />} /></CustomTooltip>
            </div>
          </Col>
        </>
      default:
        return null;
    }
  }

  // AXIOS get one user
  const getOne = (id) => {
    return (
      updateUserRequest(id).then(function (response) {
        // console.log(response);
        setFormInitValue(response.data);

        setTimeout(() => {
          setIsLoading(false);
        }, 500)

      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
    )
  }

  const pageHeader = () => {
    return (
      <>
        <PageHeader
          className="pl-0 pr-0"
          ghost={false}
          onBack={() => window.history.back()}
          title="Nazad"
          extra={[
            <Button
              key="1"
              htmlType="submit"
              type="primary"
            >
              Sačuvaj
              <BsSave />
            </Button>,
          ]}
        ></PageHeader>
      </>
    );
  };

  const editUserForm = () => {
    // console.log('user data', userData.name);
    return (
      <Formik
        initialValues={formInitValues}
        enableReinitialize={true}
        onSubmit={(values, actions) => {
          console.log(values);
          // Axios send data
          updateUserRequest(userId, values).then(function (response) {
            // handle success
            console.log(response);

            // <CustomSuccessNotification  />
 
            notification.open({
              message: 'Izmena korisnika',
              description:
                'Uspešno ste izmenili podatke.',
              icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
            });

          }).catch(function (error) {
            // handle error
            console.log(error);

            notification.open({
              message: 'Izmena korisnika',
              description:error.errors.email,
              icon: <CheckCircleOutlined style={{ color: '#46b450' }} />,
            });
          })

          actions.setSubmitting(false);
        }}
      >
        {(props, handleSubmit, setFieldValue) => (
          <Form>
            {pageHeader()}
            <Row gutter={[24, 24]}>
              <Col span={8}>
                <Field name="name" type="text" label="Ime i prezime" component={CustomInput} />
              </Col>

              <Col span={8}>
                <Field name="email" type="email" label="Email" component={CustomInput} />
              </Col>

              <Col span={8}>
                <Field name="phone" type="tel" label="Telefon" component={CustomInput} />
              </Col>

              <Col span={8}>
                <label htmlFor="role">Uloga</label>
                <CustomSelect
                  style={{ width: '100%' }}
                  showSearch
                  id="role"
                  defaultValue="super_admin"
                  onChange={(event) => {
                    // console.log(event);
                    setIsRole(event);
                  }}
                  optionFilterProp="children">
                  <Option value="super_admin">Super admin</Option>
                  <Option value="admin">Admin</Option>
                  <Option value="korisnik">Korisnik</Option>
                  <Option value="urednik_kajrona">Urednik Kajrona</Option>
                  <Option value="oglasivac">Oglašivač</Option>
                </CustomSelect>
              </Col>
              {handleRole()}
            </Row>

          </Form>
        )}
      </Formik>
    );
  };


  return <>

    <Skeleton active loading={isLoading} paragraph={{ rows: 6 }}>
      <CustomTopInfo>Izmena korisnika</CustomTopInfo>

      <section className='card-box'>
        {editUserForm()}
      </section>
    </Skeleton>
  </>;
};

