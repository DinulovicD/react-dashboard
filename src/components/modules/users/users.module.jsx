import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { AllUsers } from "./pages/all-users.page";
import { AddUser } from "./pages/add-user.page";
import { EditUser } from "./pages/edit-user.page";

export const Users = () => { 
  return (
    <Routes> 
      <Route path="/">
        <Route path="/" element={<AllUsers />} />
        <Route path="/create" element={<AddUser />} />
        <Route path="/edit/:id" element={<EditUser />} />
      </Route>
    </Routes>
  );
};
