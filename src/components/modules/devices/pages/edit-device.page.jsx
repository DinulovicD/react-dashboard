import React, { useEffect, useRef, useState } from 'react';

//Antd
import { Button, Select, Col, notification, PageHeader, Row, Spin } from 'antd';

//Icons
import { BsSave } from "react-icons/bs";

//Formik
import { Formik, Form, Field } from 'formik';

//Custom components
import CustomInput from '../../../UI/atoms/CustomInput';
import CustomSelect from '../../../UI/atoms/CustomSelect';
import CustomMap from '../../../UI/atoms/CustomMap';

//Qyill
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';


export const EditDevice = () => {

  const { Option } = Select;
  const [value, setValue] = useState('');

  const submitForm = useRef(null);

  const header = () => {
    return (
      <>
        <PageHeader
          className="pl-0 pr-0"
          ghost={false}
          onBack={() => window.history.back()}
          title="Nazad"
          extra={[
            <Button
              key="1"
              type="primary"
            >
              Sačuvaj
              <BsSave />
            </Button>,
          ]}
        ></PageHeader>
      </>
    );
  };

  const editDeviceForm = () => {
    return (
      <Formik
        initialValues={{
          device_name: '',
          device_serial_number: '',
          device_name: '',
          device_pin: '',
          device_heartbeat: '',
          device_mac_address: '',
          device_group: '',
          device_type: '',
          device_address: '',
          device_city: '',
          device_lat: '',
          device_lng: '',
        }}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            console.log(JSON.stringify(values, null, 2));
            notification.open({
              message: 'Notification Title',
              description: JSON.stringify(values, null, 2),
            });
            actions.setSubmitting(false);
          }, 1000);
        }}
      >
        {(props, handleSubmit, setFieldValue) => (
          <Form>
            <Row gutter={[24, 24]}>
              <Col span={8}>
                <Field name="device_name" type="text" label="Naziv uređaja" component={CustomInput} />
              </Col>

              <Col span={8}>
                <Field name="device_serial_number" type="text" label="Serijski broj" component={CustomInput} />
              </Col>

              <Col span={8}>
                <Field name="device_name" type="text" label="Grupa" component={CustomInput} />
              </Col>

              <Col span={8}>
                <Field name="device_pin" type="number" label="Pin" component={CustomInput} />
              </Col>

              <Col span={8}>
                <Field name="device_heartbeat" type="number" label="Heartbeat" component={CustomInput} />
              </Col>

              <Col span={8}>
                <Field name="device_mac_address" disabled type="text" label="MAC adresa" component={CustomInput} />
              </Col>

              <Col span={8}>
                <label htmlFor="">Grupa</label>
                <CustomSelect
                  style={{ width: '100%' }}
                  showSearch name="device_group" >
                  <Option value="Jack">Jack</Option>
                  <Option value="Dzon">Dzon</Option>
                  <Option value="Dzon">Dzon</Option>
                  <Option value="Dzon">Dzon</Option>
                  <Option value="Dzon">Dzon</Option>
                </CustomSelect>
              </Col>

              <Col span={8}>
                <label htmlFor="">Tip</label>
                <CustomSelect
                  style={{ width: '100%' }}
                  showSearch
                  name="device_type">
                  <Option value="Jack">Box</Option>
                  <Option value="Dzon">Telefon</Option>
                  <Option value="Dzon">TV</Option>
                  <Option value="Dzon">Tablet</Option>
                </CustomSelect>
              </Col>

              <Col span={8}>
                <label htmlFor="">Widgeti aktivni</label>
                <CustomSelect
                  style={{ width: '100%' }}
                  showSearch
                  name="device_widget"
                >
                  <Option value="Jack">Da</Option>
                  <Option value="Dzon">Ne</Option>
                </CustomSelect>
              </Col>

              <Col span={24}>
                <label htmlFor="">Beleške</label>
                <ReactQuill theme="snow" value={value} onChange={setValue} />
              </Col>

              <Col span={12}>
                <Field name="device_address" type="text" id="device_address" label="Pretraži adresu" component={CustomInput} />
                <Field name="device_city" type="hidden" id="device_city" component={CustomInput} />
              </Col>
              <Col span={6}>
                <Field name="device_lat" disabled type="text" label="Lat" id="device_lat" component={CustomInput} />
              </Col>
              <Col span={6}>
                <Field name="device_lng" disabled type="text" label="Long" id="device_lng" component={CustomInput} />
              </Col>

              <Col span={24}>
                <CustomMap setFieldValue={setFieldValue}></CustomMap>
              </Col>
            </Row>

          </Form>
        )}
      </Formik>
    );
  };


  return (
    <>
      <CustomTopInfo>Izmeni uređaja</CustomTopInfo>

      <section className='card-box'>
        {header()}
        {editDeviceForm()}
      </section>
    </>
  );
};

