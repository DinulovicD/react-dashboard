import React, { useState } from 'react';
import { Link } from 'react-router-dom';

//Ant 
import { Table, Select, Row, Col, Input, Button, Space } from 'antd';

//Icons
import {
  ReloadOutlined,
  DeleteOutlined,
  EditOutlined,
  QuestionCircleOutlined
} from '@ant-design/icons';
import { AiOutlinePlus } from "react-icons/ai";
import { BiMoviePlay } from 'react-icons/bi';
import { MdOutlineAdminPanelSettings } from "react-icons/md";

// Custom components
import CustomSelect from '../../../UI/atoms/CustomSelect';
import CustomSearchInput from '../../../UI/atoms/CustomSearchInput';
import CustomTooltip from '../../../UI/atoms/CustomTooltip';
import CustomPopConfirm from '../../../UI/atoms/CustomPopConfirm';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';

export const AllDevices = () => {


  const { Option } = Select;
  const { Search } = Input;

  //Use states
  const [filters, setFilters] = useState({});

  const columns = [
    {
      title: 'Naziv',
      dataIndex: 'naziv',
      key: 'naziv',
    },
    {
      title: 'Tip',
      dataIndex: 'tip',
      key: 'tip',
    },
    {
      title: 'Adresa',
      dataIndex: 'adresa',
      key: 'adresa',
    },
    {
      title: 'Grupa',
      dataIndex: 'grupa',
      key: 'grupa',
    },
    {
      title: 'Plejlista',
      dataIndex: 'plejlista',
      key: 'plejlista',
    },
    {
      title: 'Akcije',
      key: 'akcije',
      width: '10%',
      render: (user) => (
        <>
          <Space size="middle">
            <CustomTooltip title="Super admin panel"><Link to="/devices/super-admin/1"><Button className="mr-2 d-flex align-center justify-center" icon={<MdOutlineAdminPanelSettings />} /></Link></CustomTooltip>

            <CustomTooltip title="Pregled celokupne playliste"><Button className="mr-2 d-flex align-center justify-center" icon={<BiMoviePlay />} /></CustomTooltip>

            <CustomTooltip title="Izmeni"><Link to="/devices/edit/1"><Button className="mr-2 d-flex align-center justify-center" icon={<EditOutlined />} /></Link></CustomTooltip>

            <CustomTooltip title="Obriši">
              <CustomPopConfirm title="Da li ste sigurni da želite da obrišete stavku?" icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                <Button className="mr-2 d-flex align-center justify-center" icon={<DeleteOutlined />} />
              </CustomPopConfirm>
            </CustomTooltip>
          </Space>
        </>
      )
    },
  ];

  const data = [
    {
      key: '1',
      naziv: 'test company fake',
      tip: 'fake test android box 1',
      adresa: 'group test fake 4',
      grupa: 'merc',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },

    {
      key: '2',
      naziv: 'test company fake',
      tip: 'fake test android box 1',
      adresa: 'group test fake 4',
      grupa: 'merc',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },

    {
      key: '3',
      naziv: 'test company fake',
      tip: 'fake test android box 1',
      adresa: 'group test fake 4',
      grupa: 'merc',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },

    {
      key: '4',
      naziv: 'test company fake',
      tip: 'fake test android box 1',
      adresa: 'group test fake 4',
      grupa: 'merc',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },

    {
      key: '5',
      naziv: 'test company fake',
      tip: 'fake test android box 1',
      adresa: 'group test fake 4',
      grupa: 'merc',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },

    {
      key: '6',
      naziv: 'test company fake',
      tip: 'fake test android box 1',
      adresa: 'group test fake 4',
      grupa: 'merc',
      plejlista: 'fake advertiser 1, fakeadvertiser1@itcentar.rs',
    },


  ];

  const resetFilters = () => {
    setFilters({});
  }



  // Device filters
  const deviceFilters = () => {
    return (
      <Row gutter={[24, 24]} className='mb-4' justify='end'>
        <Col flex="auto" className='d-flex'>
          <Link to="/devices/create"><Button type="primary" className='d-flex align-center mr-3'> Dodaj novi uređaj <AiOutlinePlus /></Button></Link>
          <Link to="/devices/edit-many"><Button type="primary" className='d-flex align-center'> Izmeni više uređaja <EditOutlined className='ml-0' /></Button></Link>
        </Col>
        <Col span={5}><CustomSearchInput placeholder="Pretraži email adrese" /></Col>
        <Col span={5}>
          <CustomSelect
            style={{ width: '100%' }}
            showSearch
            placeholder="Izaberite kompaniju"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            } >
            <Option value="Jack">Jack</Option>
            <Option value="Dzon">Dzon</Option>
          </CustomSelect>
        </Col>
        <Col span={4}>
          <CustomSelect
            style={{ width: '100%' }}
            showSearch
            placeholder="Izaberite ulogu"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            } >
            <Option value="Jack">Jack</Option>
            <Option value="Dzon">Dzon</Option>
          </CustomSelect>
        </Col>
        <Col><Button icon={<ReloadOutlined />} onClick={() => resetFilters()} /></Col>
      </Row>
    )
  }

  // Device table
  const deviceTable = () => {
    return (
      <Table columns={columns} bordered dataSource={data} />
    )
  }


  return <>

    <CustomTopInfo>LISTA POJEDINAČNIH UREĐAJA</CustomTopInfo>

    <section className='card-box'>

      {deviceFilters()}

      {deviceTable()}
    </section>
  </>;
};
