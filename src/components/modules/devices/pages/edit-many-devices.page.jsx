import React, { useEffect, useRef, useState } from 'react';

//Ant
import { Button, Select, Col, notification, PageHeader, Row, Spin, Modal } from 'antd';

//Formik
import { Formik, Form, Field } from 'formik';

//Icons
import { AiOutlineInfoCircle } from "react-icons/ai";
import { BsSave } from "react-icons/bs";

//Custom components
import CustomTooltip from '../../../UI/atoms/CustomTooltip';
import CustomCheckbox from '../../../UI/atoms/CustomCheckbox';
import CustomSelect from '../../../UI/atoms/CustomSelect';
import CustomTopInfo from '../../../UI/molecules/CustomTopInfo';


export const EditManyDevices = () => {

  const { Option } = Select;
  const [value, setValue] = useState('');
  const [formInitValues, setFromInitValues] = useState({
    name: 'MAX',
    email: '',
    password: '',
    password_confirmation: '',
    phone: '',
    role_id: '',
  });

  const submitForm = useRef(null);

  const [isModalVisible, setIsModalVisible] = useState(false);


  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const header = () => {
    return (
      <>
        <PageHeader
          className="pl-0 pr-0"
          ghost={false}
          onBack={() => window.history.back()}
          title="Nazad"
          extra={[
            <Button
              key="1"
              type="primary"
            >
              Sačuvaj
              <BsSave />
            </Button>,
          ]}
        ></PageHeader>
      </>
    );
  };

  const editManyDeviceForm = () => {
    return (
      <Formik
        initialValues={{
          app_file: '',
          app_version: '',
          app_description: '',
        }}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            console.log(JSON.stringify(values, null, 2));
            notification.open({
              message: 'Notification Title',
              description: JSON.stringify(values, null, 2),
            });
            actions.setSubmitting(false);
          }, 1000);
        }}
      >
        {(props, handleSubmit) => (
          <Form>
            <Row gutter={[24, 24]}>

              <Col span={12}>
                <label htmlFor="">Grupa</label>
                <CustomSelect
                  style={{ width: '100%' }}
                  showSearch  >
                  <Option value="Jack">Jack</Option>
                  <Option value="Dzon">Dzon</Option>
                  <Option value="Dzon">Dzon</Option>
                  <Option value="Dzon">Dzon</Option>
                  <Option value="Dzon">Dzon</Option>
                </CustomSelect>
              </Col>

              <Col span={12}>
                <label htmlFor="">Widgeti aktivni</label>
                <CustomSelect
                  style={{ width: '100%' }}
                  showSearch  >
                  <Option value="Jack">Da</Option>
                  <Option value="Dzon">Ne</Option>
                </CustomSelect>
              </Col> 

              <Col span={24}>
                <h2 className='mb-0 card-title'>uređaji</h2>
              </Col>

              <Col span={6}>
                <div className="edit-many-devices-list">
                  <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
                  <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" onClick={showModal} icon={<AiOutlineInfoCircle />} /></CustomTooltip>
                </div>
              </Col>

              <Col span={6}>
                <div className="edit-many-devices-list">
                  <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
                  <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" onClick={showModal} icon={<AiOutlineInfoCircle />} /></CustomTooltip>
                </div>
              </Col>

              <Col span={6}>
                <div className="edit-many-devices-list">
                  <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
                  <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" onClick={showModal} icon={<AiOutlineInfoCircle />} /></CustomTooltip>
                </div>
              </Col>

              <Col span={6}>
                <div className="edit-many-devices-list">
                  <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
                  <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" onClick={showModal} icon={<AiOutlineInfoCircle />} /></CustomTooltip>
                </div>
              </Col>

              <Col span={6}>
                <div className="edit-many-devices-list">
                  <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
                  <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" onClick={showModal} icon={<AiOutlineInfoCircle />} /></CustomTooltip>
                </div>
              </Col>

              <Col span={6}>
                <div className="edit-many-devices-list">
                  <CustomCheckbox label="Samsung telefon 1"></CustomCheckbox>
                  <CustomTooltip title="Informacije"><Button className="mr-2 d-flex align-center justify-center" onClick={showModal} icon={<AiOutlineInfoCircle />} /></CustomTooltip>
                </div>
              </Col>


            </Row>

          </Form>
        )}
      </Formik>
    );
  };

  const editManyDeviceModal = () => {
    return (
      <Modal centered title="Informacije uređaj" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <ul>
          <li><strong>Naziv uređaja: </strong> Box1</li>
          <li><strong>Tip uređaja: </strong> Box</li>
          <li><strong>Lokacija: </strong> Cara Dušana, Novi Sad, Serbia</li>
          <li><strong>Trenutna grupa: </strong> ZaDjoletaTest</li>
        </ul>
      </Modal>
    )
  }


  return (
    <>

<CustomTopInfo>Izmeni više uređaja</CustomTopInfo>


      <section className='card-box'>
        {header()}
        {editManyDeviceForm()}
        {editManyDeviceModal()}
      </section>
    </>
  );
};
