import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { AllDevices } from "./pages/all-devices.page";
import { AddDevice } from "./pages/add-device.page";
import { EditDevice } from "./pages/edit-device.page";
import { EditManyDevices } from "./pages/edit-many-devices.page";

export const Devices = () => { 
  return (
    <Routes> 
      <Route path="/">
        <Route path="/" element={<AllDevices />} />
        <Route path="/create" element={<AddDevice />} />
        <Route path="/edit/:id" element={<EditDevice />} />
        <Route path="/edit-many/" element={<EditManyDevices />} />
      </Route>
    </Routes>
  );
};
